#include "pixel.h"
#include <iostream>

Pixel::Pixel() : _caractere1(' '), _caractere2(' '), _couleur(Pixel::WBLACK) {}

Pixel::Pixel(Color couleur) : _caractere1(' '), _caractere2(' '),  _couleur(couleur) {}

Pixel::Pixel(Color couleur, char caractere1) : _couleur(couleur)
{
  setCaracteres(caractere1, caractere1);
}

Pixel::Pixel(Color couleur, char caractere1, char caractere2) : _couleur(couleur)
{
  setCaracteres(caractere1, caractere2);
}

std::vector<Pixel> Pixel::texteInPixels (const std::string& texte, Pixel::Color couleur)
{
  size_t length = texte.length();
  std::vector<Pixel> pixels;
  char cara2;
  
  for (size_t i = 0; i < length; i+=2)
  {
    if (i+1 < length) cara2 = texte.at(i+1);
    else cara2 = ' ';
    
    pixels.push_back(Pixel(couleur, texte.at(i), cara2));
  }
  
  return pixels;
}
  
void Pixel::setCaracteres(char caractere1, char caractere2)
{
  if (caractere1 >= 32 && caractere1 < 127)
  {
    _caractere1 = caractere1;
  }
  else 
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Le caractère 1 est invalide, il ne fait pas partie des caractères ASCII standard !"<<std::endl;
    _caractere1 = ' ';
  }
  
  if (caractere2 >= 32 && caractere2 < 127)
  {
    _caractere2 = caractere2;
  }
  else 
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Le caractère 2 est invalide, il ne fait pas partie des caractères ASCII standard !"<<std::endl;
    _caractere2 = ' ';
  }
}

void Pixel::setCouleur(Pixel::Color couleur)
{
  _couleur = couleur;
}
  
char Pixel::getCaractere1() const
{
  return _caractere1;
}

char Pixel::getCaractere2() const
{
  return _caractere2;
}

std::string Pixel::getCaracteres() const
{
  std::string retour;
  retour += _caractere1;
  retour += _caractere2;
  return retour;
}

Pixel::Color Pixel::getCouleur() const
{
  return _couleur;
}