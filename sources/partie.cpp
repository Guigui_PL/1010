#include "partie.h"
#include <exception>
#include <iostream>
#include <fstream>
#include "grille.h"
#include "score.h"
#include "piecesManager.h"

using namespace std;


Partie::Partie() : _posHistoricGrille(NULL), _posListScore(NULL), _posHistoricChoicesPiece(NULL){}
Partie::Partie(PiecesManager* piecesM, Score* score, Grille* grille) : _grille(grille), _posHistoricGrille(NULL), _piecesM(piecesM), _score(score), _posListScore(NULL), _posHistoricChoicesPiece(NULL) {}
Partie::Partie(PiecesManager& piecesM, Score& score, Grille& grille) : _grille(&grille), _posHistoricGrille(NULL), _piecesM(&piecesM), _score(&score), _posListScore(NULL), _posHistoricChoicesPiece(NULL) {}

Grille* Partie::getGrille() const { return _grille; }
const ListeDC<Grille>& Partie::getHistoricGrille() const { return _historicGrille; }
ListeDC<Grille>::Element Partie::getPosHistoricGrille() const { return _posHistoricGrille; }
PiecesManager* Partie::getPiecesM() const { return _piecesM; }
Score* Partie::getScore() const { return _score; }
const ListeDC<std::array<size_t,2>>& Partie::getListScore() const { return _listScore; }
ListeDC<std::array<size_t,2>>::Element Partie::getPosListScore() const { return _posListScore; }
const ListeDC<std::array<size_t,3>>& Partie::getHistoricChoicesPiece() const { return _historicChoicesPiece; }
ListeDC<std::array<size_t,3>>::Element Partie::getPosHistoricChoicesPiece() const { return _posHistoricChoicesPiece; }

void Partie::setGrille(Grille* grille) { _grille = grille; }
void Partie::setGrille(Grille& grille) { _grille = &grille; }
void Partie::setHistoricGrille(const ListeDC<Grille>& historicGrille) { _historicGrille = historicGrille; }
void Partie::setPosHistoricGrille(const ListeDC<Grille>::Element& posHistoricGrille) { _posHistoricGrille = posHistoricGrille; }
void Partie::setPiecesM(PiecesManager* piecesM) { _piecesM = piecesM; }
void Partie::setPiecesM(PiecesManager& piecesM) { _piecesM = &piecesM; }
void Partie::setScore(Score* score) { _score = score; }
void Partie::setScore(Score& score) { _score = &score; }
void Partie::setListScore(const ListeDC<std::array<size_t,2>>& listScore) { _listScore = listScore; }
void Partie::setPosListScore(const ListeDC<std::array<size_t,2>>::Element& posListScore) { _posListScore = posListScore; }
void Partie::setHistoricChoicesPiece(const ListeDC<std::array<size_t,3>>& historicChoicesPiece) { _historicChoicesPiece = historicChoicesPiece; }
void Partie::setPosHistoricChoicesPiece(const ListeDC<std::array<size_t,3>>::Element& posHistoricChoicesPiece) { _posHistoricChoicesPiece = posHistoricChoicesPiece; }

void Partie::addHistoricGrille(const Grille* grille)
{ 
	if (_posHistoricGrille != _historicGrille.dernier()) 
	{
		_historicGrille.supprItv( _posHistoricGrille->getSucc(), _historicGrille.dernier());
	}
	
	_historicGrille.ajoutFin(*grille);

	_posHistoricGrille = _historicGrille.dernier();
}

void Partie::addScore(const Score* score)
{ 
	if (_posListScore != _listScore.dernier()) 
	{
		_listScore.supprItv( _posListScore->getSucc(), _listScore.dernier());
	}
	
	_listScore.ajoutFin({score->getTotal(), score->getMeilleur()});

	_posListScore = _listScore.dernier();
}

void Partie::addHistoricChoicesPiece(const std::array<size_t,3>& pieces)
{ 
	if (_posHistoricChoicesPiece != _historicChoicesPiece.dernier()) 
	{
		_historicChoicesPiece.supprItv( _posHistoricChoicesPiece->getSucc(), _historicChoicesPiece.dernier());
	}
	
	_historicChoicesPiece.ajoutFin(pieces);

	_posHistoricChoicesPiece = _historicChoicesPiece.dernier();
}

void Partie::back ()
{
  backHistoricGrille();
  backListScore();
  backHistoricChoicesPiece();
}

void Partie::backHistoricGrille()
{
	if (_posHistoricGrille != NULL)
	{
		if (_posHistoricGrille != _historicGrille.premier())
		{
			_posHistoricGrille = _historicGrille.pred(_posHistoricGrille);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas retourner en arrière, la position est nulle !" << endl;
		terminate();
	}
	*_grille = _historicGrille.info(_posHistoricGrille);
}

void Partie::backListScore()
{
	if (_posListScore != NULL)
	{
		if (_posListScore != _listScore.premier())
		{
			_posListScore = _listScore.pred(_posListScore);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas retourner en arrière, la position est nulle !" << endl;
		terminate();
	}
	_score->setTotal(_listScore.info(_posListScore)[0]);
	_score->setMeilleur(_listScore.info(_posListScore)[1]);
}

void Partie::backHistoricChoicesPiece()
{
	if (_posHistoricChoicesPiece != NULL)
	{
		if (_posHistoricChoicesPiece != _historicChoicesPiece.premier())
		{
			_posHistoricChoicesPiece = _historicChoicesPiece.pred(_posHistoricChoicesPiece);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas retourner en arrière, la position est nulle !" << endl;
		terminate();
	}
	_piecesM->setPiecesDispos(_historicChoicesPiece.info(_posHistoricChoicesPiece));
}

void Partie::next ()
{
  nextHistoricGrille();
  nextListScore();
  nextHistoricChoicesPiece();
}

void Partie::nextHistoricGrille()
{
	if (_posHistoricGrille != NULL)
	{
		if (_posHistoricGrille != _historicGrille.dernier())
		{
			_posHistoricGrille = _historicGrille.succ(_posHistoricGrille);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas aller en avant, la position est nulle !" << endl;
		terminate();
	}
	*_grille = _historicGrille.info(_posHistoricGrille);
}

void Partie::nextListScore()
{
	if (_posListScore != NULL)
	{
		if (_posListScore != _listScore.dernier())
		{
			_posListScore = _listScore.succ(_posListScore);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas aller en avant, la position est nulle !" << endl;
		terminate();
	}
	_score->setTotal(_listScore.info(_posListScore)[0]);
	_score->setMeilleur(_listScore.info(_posListScore)[1]);
}

void Partie::nextHistoricChoicesPiece()
{
	if (_posHistoricChoicesPiece != NULL)
	{
		if (_posHistoricChoicesPiece != _historicChoicesPiece.dernier())
		{
			_posHistoricChoicesPiece = _historicChoicesPiece.succ(_posHistoricChoicesPiece);
		}
	}
	else
	{
		cerr << __FILE__ << " " << __LINE__ << " : Vous ne pouvez pas aller en avant, la position est nulle !" << endl;
		terminate();
	}
	_piecesM->setPiecesDispos(_historicChoicesPiece.info(_posHistoricChoicesPiece));
}

void Partie::update()
{
	addHistoricGrille(_grille);
	addScore(_score);
	addHistoricChoicesPiece(_piecesM->getPiecesDispos());
}

/*
 *Save the game
 *Param STRING which represents the name that the user gave to the game
 *Return VOID
 */
void Partie::save(string name)
{
	string fullpath = "saves/games/" + name;
	ofstream saving(fullpath);
	
	ListeDC<Grille>::Element g = _posHistoricGrille;
	ListeDC<std::array<size_t,2>>::Element s = _posListScore;
	ListeDC<array<size_t,3>>::Element p = _posHistoricChoicesPiece;

	if(saving.is_open())
	{
		saving << _piecesM->getConfigFile() << endl;
		saving << _score->getNomJoueur();
		
		size_t height = _grille->getHauteur();
		size_t width = _grille->getLargeur();
		
		ListeDC<Grille>::Element g = _posHistoricGrille;
		ListeDC<array<size_t,2>>::Element s = _posListScore;
		ListeDC<array<size_t,3>>::Element p = _posHistoricChoicesPiece;
	
		while(g != NULL)
		{
			saving << endl;
			
			saving << s->getInfo()[0] << " " << s->getInfo()[1] << endl;
	
			for(size_t h = 0; h < height; h++)
			{
				for(size_t w = 0; w < width; w++)
				{
					Case c = g->getInfo().getCase(h,w);
					int color = 0;
					if(!c.estVide())
						color = c.getCouleur();
	
					saving << color << " ";
				}	
	
				saving << endl;
			}
	
			array<size_t,3> h_disp = p->getInfo();
	
			for( size_t i = 0; i < 3; i++)
			{
				if(h_disp[i] == static_cast<size_t>(-1))
					saving << -1 << " ";
				else
					saving << h_disp[i] << " ";
			}
	
			g = _historicGrille.pred(g);
			s = _listScore.pred(s);
			p = _historicChoicesPiece.pred(p);
		}
	
		saving.close();
	}

}

void Partie::load(string filename)
{
	ifstream saving(filename, std::ios::in);
	
	string s_container;

	if(saving.is_open())
	{
		getline(saving,s_container);
		_piecesM->loadConf(s_container);

		string name;
		
		getline(saving, name);
		array<size_t,2> a_score;
		saving >> a_score[0];
		saving >> a_score[1];

		*_score = Score();
		_score->setTotal(a_score[0]);
		_score->setMeilleur(a_score[1]);
		_score->setNomJoueur(name);
		
		_listScore.ajoutDebut(a_score);
		_posListScore = _listScore.premier();

		*_grille = Grille(_score);

		for(short int h = 0; h < 10; h++)
		{
			for( short int w = 0; w < 10; w++)
			{
				int c;
				saving >> c;
				Case c_case;
				if(c==0)
				{
					c_case.setVide(true);
					c_case.setCouleur(Case::Couleur(0));
				}
				else
				{
					c_case.setVide(false);
					c_case.setCouleur(Case::Couleur(c));
				}
				_grille->setCase(h,w, c_case);	
			}
		}

		_historicGrille.ajoutDebut(*_grille);
		_posHistoricGrille = _historicGrille.premier();

		array<size_t,3> pieceDisp;

		for(short int p = 0; p < 3; p++)
		{
			saving >> pieceDisp[p];
		}

		_piecesM->setPiecesDispos(pieceDisp);

		_historicChoicesPiece.ajoutDebut(pieceDisp);

		_posHistoricChoicesPiece = _historicChoicesPiece.premier();

		while(!saving.eof())
		{
			saving >> a_score[0];
			saving >> a_score[1];
			_listScore.ajoutDebut(a_score);
			Grille g(_score);


			for( short int h = 0; h < 10; h++)
			{
				for( short int w = 0; w < 10; w++)
				{
					int c;
					saving >> c;
					Case c_case;
					if(c==0)
					{
						c_case.setVide(true);
						c_case.setCouleur(Case::Couleur(0));
					}
					else
					{
						c_case.setVide(false);
						c_case.setCouleur(Case::Couleur(c));
					}
					g.setCase(h,w, c_case);
				}
			}

			_historicGrille.ajoutDebut(g);

			for (short int p =0; p < 3; p++)
			{
				saving >> pieceDisp[p];
			}

			_historicChoicesPiece.ajoutDebut(pieceDisp);
		}
		
		saving.close();
	}
	else
	{
	  cerr << __FILE__ << " " << __LINE__ << " : la sauvegarde " << filename << " n'a pas pu être chargée !" << endl;
	  terminate();
	}
}
