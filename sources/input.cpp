#include "input.h"
#include <string>
#include <algorithm>
#include <ctime>
#include <dirent.h>
#include <sys/types.h>
#include <curses.h>
#include "piecesManager.h"
#include "grille.h"
#include "score.h"

#define DOSSIER_SAUVEGARDES "saves/games"

using namespace std;


Input::Input() : _piecesM(NULL), _grille(NULL), _score(NULL), _affichage(NULL)
{
  *_verrClavier = false;
}

Input::Input(Grille* grille, Score* score, PiecesManager* piecesM, Affichage* affichage, bool* verrClavier) :
  _piecesM(piecesM),
  _grille(grille),
  _score(score),
  _affichage(affichage),
  _verrClavier(verrClavier)
{
  menuPrincipal();
}

const MenuManager& Input::getMenuM() const { return _menuM; }
const Partie& Input::getPartie() const { return _partie;}
PiecesManager* Input::getPiecesM() const { return _piecesM;}
Score* Input::getScore() const { return _score;}
Affichage* Input::getAffichage() const { return _affichage;}

void Input::setMenuM(const MenuManager& menuM ) { _menuM = menuM; }
void Input::setPartie(const Partie& partie) { _partie = partie; }
void Input::setPiecesM(PiecesManager* piecesM) { _piecesM = piecesM; }
void Input::setPiecesM(PiecesManager& piecesM) { _piecesM = &piecesM; }
void Input::setScore(Score* score) { _score = score; }
void Input::setScore(Score& score) { _score = &score; }
void Input::setAffichage(Affichage* affichage) { _affichage = affichage; }
void Input::setAffichage(Affichage& affichage) { _affichage = &affichage; }

void Input::chargeMenuAccueil ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Nouvelle partie", 'N', "Commencer une nouvelle partie"));
  _menuM.addOptionMenu(Menu(1, "Nouvelle partie", 'n', "Commencer une nouvelle partie"));
  _menuM.addOptionMenu(Menu(2, "Reprendre partie", 'R', "Continuer une partie precedente"));
  _menuM.addOptionMenu(Menu(3, "Reprendre partie", 'r', "Continuer une partie precedente"));
  _menuM.addOptionMenu(Menu(4, "Credits", 'C', "Voir les credits"));
  _menuM.addOptionMenu(Menu(5, "Credits", 'c', "Voir les credits"));
  _menuM.addOptionMenu(Menu(6, "Quitter", 27, "Quitter le jeu"));
}

void Input::chargeMenuPartie ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter la partie"));
  _menuM.addOptionMenu(Menu(1, "Piece 1", '1', "Prendre la premiere piece"));
  _menuM.addOptionMenu(Menu(2, "Piece 1", '&', "Prendre la premiere piece"));
  _menuM.addOptionMenu(Menu(3, "Piece 2", '2', "Prendre la seconde piece"));
  _menuM.addOptionMenu(Menu(4, "Piece 3", '3', "Prendre la troisieme piece"));
  _menuM.addOptionMenu(Menu(5, "Piece 3", '"', "Prendre la troisieme piece"));
  _menuM.addOptionMenu(Menu(6, "Meilleurs scores", 'B', "Voir les meilleurs scores"));
  _menuM.addOptionMenu(Menu(7, "Meilleurs scores", 'b', "Voir les meilleurs scores"));
  _menuM.addOptionMenu(Menu(8, "Annuler", '4', "Annuler la derniere action"));
  _menuM.addOptionMenu(Menu(9, "Refaire", '6', "Refaire la derniere action"));
  _menuM.addOptionMenu(Menu(10, "Souris Partie", KEY_MOUSE, "Souris Partie"));
}

void Input::chargeMenuPlacement ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter la partie"));
  _menuM.addOptionMenu(Menu(1, "Haut", KEY_UP, "Deplacer la piece vers le haut"));
  _menuM.addOptionMenu(Menu(2, "Bas", KEY_DOWN, "Deplacer la piece vers le bas"));
  _menuM.addOptionMenu(Menu(3, "Gauche", KEY_LEFT, "Deplacer la piece vers la gauche"));
  _menuM.addOptionMenu(Menu(4, "Droite", KEY_RIGHT, "Deplacer la piece vers la droite"));
  _menuM.addOptionMenu(Menu(5, "Poser Piece", KEY_ENTER, "Poser la piece"));
  _menuM.addOptionMenu(Menu(6, "Poser Piece", 13, "Poser la piece"));
  _menuM.addOptionMenu(Menu(7, "Poser Piece", 10, "Poser la piece"));
  //Changement de pièce a la volée
  _menuM.addOptionMenu(Menu(8, "Piece 1", '1', "Prendre la premiere piece"));
  _menuM.addOptionMenu(Menu(9, "Piece 1", '&', "Prendre la premiere piece"));
  _menuM.addOptionMenu(Menu(10, "Piece 2", '2', "Prendre la seconde piece"));
  _menuM.addOptionMenu(Menu(11, "Piece 3", '3', "Prendre la troisieme piece"));
  _menuM.addOptionMenu(Menu(12, "Piece 3", '"', "Prendre la troisieme piece"));
  _menuM.addOptionMenu(Menu(13, "Saisir coordonnees", '5', "Saisir les coordonnees de la piece sur le clavier"));
  _menuM.addOptionMenu(Menu(14, "Souris Placement", KEY_MOUSE, "Souris Placement"));
}

void Input::chargeMenuSaisieCoor ()
{
  _menuM = MenuManager(); // On vide le menu
  
  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter la partie"));
  _menuM.addOptionMenu(Menu(1, "Confirmer", 10, "Confirmer la saisie"));
  _menuM.addOptionMenu(Menu(2, "coordonnee 0", '0', "modifie la coordonnee à 0"));
  _menuM.addOptionMenu(Menu(3, "coordonnee 1", '1', "modifie la coordonnee à 1"));
  _menuM.addOptionMenu(Menu(4, "coordonnee 2", '2', "modifie la coordonnee à 2"));
  _menuM.addOptionMenu(Menu(5, "coordonnee 3", '3', "modifie la coordonnee à 3"));
  _menuM.addOptionMenu(Menu(6, " ", ' ', " "));
  _menuM.addOptionMenu(Menu(7, "coordonnee 4", '4', "modifie la coordonnee à 4"));
  _menuM.addOptionMenu(Menu(8, "coordonnee 5", '5', "modifie la coordonnee à 5"));
  _menuM.addOptionMenu(Menu(9, "coordonnee 6", '6', "modifie la coordonnee à 6"));
  _menuM.addOptionMenu(Menu(10, "coordonnee 7", '7', "modifie la coordonnee à 7"));
  _menuM.addOptionMenu(Menu(11, "coordonnee 8", '8', "modifie la coordonnee à 8"));
  _menuM.addOptionMenu(Menu(12, "coordonnee 9", '9', "modifie la coordonnee à 9"));
}

void Input::chargeMenuSaisiePseudo()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter le jeu"));
  _menuM.addOptionMenu(Menu(1, "Confirmer pseudo", KEY_ENTER, "Confirmer le choix de pseudo"));
  _menuM.addOptionMenu(Menu(2, "Confirmer pseudo", 10, "Confirmer le choix de pseudo"));
  _menuM.addOptionMenu(Menu(3, "Confirmer pseudo", 13, "Confirmer le choix de pseudo"));
  _menuM.addOptionMenu(Menu(4, "Effacer", KEY_BACKSPACE, "Effacer une lettre"));
}

void Input::chargeMenuBestScoresPartie()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter la partie"));
  _menuM.addOptionMenu(Menu(1, "Reprendre", KEY_ENTER, "Reprendre la partie"));
  _menuM.addOptionMenu(Menu(2, "Reprendre", 10, "Reprendre la partie"));
  _menuM.addOptionMenu(Menu(3, "Reprendre", 13, "Reprendre la partie"));
}

void Input::chargeMenuBestScoresGameOver()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 27, "Quitter le jeu"));
  _menuM.addOptionMenu(Menu(1, "Menu principal", KEY_ENTER, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(2, "Menu principal", 10, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(3, "Menu principal", 13, "Retourner au menu principal"));
}


void Input::chargeMenuDemandeSauvegarde()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Quitter", 'N', "Quitter sans sauvegarder"));
  _menuM.addOptionMenu(Menu(1, "Quitter", 'n', "Quitter sans sauvegarder"));
  _menuM.addOptionMenu(Menu(2, "Sauvegarder", 'y', "Sauvegarder la partie puis quitter"));
  _menuM.addOptionMenu(Menu(3, "Sauvegarder", 'Y', "Sauvegarder la partie puis quitter"));
}

void Input::chargeMenuNomSauvegarde ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Confirmer sauvegarde", KEY_ENTER, "Confirmer le nom de la sauvegarde"));
  _menuM.addOptionMenu(Menu(1, "Confirmer sauvegarde", 10, "Confirmer le nom de la sauvegarde"));
  _menuM.addOptionMenu(Menu(2, "Confirmer sauvegarde", 13, "Confirmer le nom de la sauvegarde"));
  _menuM.addOptionMenu(Menu(3, "Effacer", KEY_BACKSPACE, "Effacer une lettre"));
}

void Input::chargeMenuGameOver ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Meilleurs scores", KEY_ENTER, "Voir les meilleurs scores"));
  _menuM.addOptionMenu(Menu(1, "Meilleurs scores", 10, "Voir les meilleurs scores"));
  _menuM.addOptionMenu(Menu(2, "Meilleurs scores", 13, "Voir les meilleurs scores"));
  _menuM.addOptionMenu(Menu(3, "Menu principal", 27, "Retourner au menu principal"));
}

void Input::chargeMenuCredits ()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Menu principal", KEY_ENTER, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(1, "Menu principal", 10, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(2, "Menu principal", 13, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(3, "Menu principal", 27, "Retourner au menu principal"));
}

void Input::chargeMenuSauvegarde()
{
  _menuM = MenuManager(); // On vide le menu

  // On insère les éléments dans le menu
  _menuM.addOptionMenu(Menu(0, "Menu principal", 27, "Retourner au menu principal"));
  _menuM.addOptionMenu(Menu(1, "Charger", KEY_ENTER, "Charger la sauvegarde"));
  _menuM.addOptionMenu(Menu(2, "Charger", 10, "Charger la sauvegarde"));
  _menuM.addOptionMenu(Menu(3, "Charger", 13, "Charger la sauvegarde"));
  _menuM.addOptionMenu(Menu(4, "Supprimer", 127, "Supprimer la sauvegarde"));
  _menuM.addOptionMenu(Menu(5, "Supprimer", KEY_DC, "Supprimer la sauvegarde"));
  _menuM.addOptionMenu(Menu(6, "Descendre", KEY_DOWN, "Descendre"));
  _menuM.addOptionMenu(Menu(7, "Monter", KEY_UP, "Monter"));
}

bool Input::catchKey (int touche)
{
  bool arret = false;
  if (_menuM.estDansMenu(touche) && !(*_verrClavier))
  {
    switch (touche)
    {
      case 'N':
      case 'n':

	if (_menuM.estDansMenu("Nouvelle partie"))
	  ecranSaisieNom();
	
	else if (_menuM.estDansMenu("Quitter sans sauvegarder"))
	  arret = true;

	break;
	
      case 'Y':
      case 'y':
	
	nomSauvegarde();
	
	break;

      case '0':
	
	MAJCoor (0);
	
	break;

      case '1':
      case '&':
	//appel des fonctions pour la pieces 1 dans pieces managers && vérifie pour le confor que la pièce n'est pas déjà selectioner
	if (_menuM.estDansMenu("Piece 1") && _pieceTenue != 0)
	  chargePlacementPiece(0);
	
	else if (_menuM.estDansMenu("coordonnee 1"))
	  MAJCoor (1);

	break;

      case '2': // On ne peut utiliser 'é', il ne fait pas partie des caractères char valides : voir table ASCII standard !!!!
	//appel des fonctions pour la pieces 2 dans pieces managers
	if (_menuM.estDansMenu("Piece 2") && _pieceTenue != 1)
	  chargePlacementPiece(1);
	
	else if (_menuM.estDansMenu("coordonnee 2"))
	  MAJCoor (2);

	break;

      case '3':
      case '"':
	//appel des fonctions pour la pieces 3 dans pieces managers
	if (_menuM.estDansMenu("Piece 3") && _pieceTenue != 2)
	  chargePlacementPiece(2);
	
	else if (_menuM.estDansMenu("coordonnee 3"))
	  MAJCoor (3);

	break;

      case '4':
	
	if (_menuM.estDansMenu("Annuler"))
	  annuler();
	
	else if (_menuM.estDansMenu("coordonnee 4"))
	  MAJCoor (4);
	
	break;

      case '5':
	if (_menuM.estDansMenu("Saisir coordonnees"))
	  saisieCoor();
	
	else if (_menuM.estDansMenu("coordonnee 5"))
	  MAJCoor (5);
	
	break;

      case '6':
	
	if (_menuM.estDansMenu("Refaire"))
	  refaire();
	
	else if (_menuM.estDansMenu("coordonnee 6"))
	  MAJCoor (6);
	
	break;

      case '7':
	
	MAJCoor (7);
	
	break;

      case '8':
	
	MAJCoor (8);
	
	break;

      case '9':

	MAJCoor (9);
	
	break;

      // https://www.gnu.org/software/guile-ncurses/manual/html_node/Getting-characters-from-the-keyboard.html
      case KEY_UP:

	if (_menuM.estDansMenu("Haut"))
	  deplacePiece(_coorPieceT.x, _coorPieceT.y - 1);
	
	else if (_menuM.estDansMenu("Monter"))
	{
	  if (_idListe > 0) --_idListe;
	  listeSauvegardes();
	}

	break;

      case KEY_DOWN:

	if (_menuM.estDansMenu("Bas"))
	  deplacePiece(_coorPieceT.x, _coorPieceT.y + 1);
	
	else if (_menuM.estDansMenu("Descendre"))
	{
	  if (_idListe < _liste.size()-1) ++_idListe;
	  listeSauvegardes();
	}

	break;

      case KEY_LEFT:

	deplacePiece(_coorPieceT.x - 1, _coorPieceT.y);

	break;

      case KEY_RIGHT:

	deplacePiece(_coorPieceT.x + 1, _coorPieceT.y);

	break;

      case 10: // Saut de ligne
      case 13: // Retour de carrière
      case KEY_ENTER:
	//appel des fonctions permettant le pacement sur la grille de la piece
	if (_menuM.estDansMenu("Poser Piece"))
	  placerPiece();
	
	else if (_menuM.estDansMenu("Confirmer pseudo"))
	  lancerNouvellePartie();
	
	else if (_menuM.estDansMenu("Reprendre"))
	  sortirMeilleursScores();
	
	else if (_menuM.estDansMenu("Confirmer sauvegarde"))
	{
	  arret = true;
	  _partie.save(_chaine + ".save");
	}
	
	else if (_menuM.estDansMenu("Charger"))
	  chargeSauvegarde();
	
	else if (_menuM.estDansMenu("Meilleurs scores"))
	{
	  chargeMenuBestScoresGameOver();
	  meilleursScores();
	}
	
	else if (_menuM.estDansMenu("Menu principal"))
	  menuPrincipal();

	break;
	
      case KEY_BACKSPACE:
	
	eraseLastChar();
	
	break;
	
      case 27: // 27 = Echap
	
	if (_menuM.estDansMenu("Quitter le jeu"))
	  arret = true;
	
	else if (_menuM.estDansMenu("Quitter la partie"))
	  demandeSauvegarde ();
	
	else if (_menuM.estDansMenu("Menu principal"))
	  menuPrincipal();
	
	break;
	
      case 'B':
      case 'b':
	
	chargeMenuBestScoresPartie();
	meilleursScores();
	
	break;
	
      case 'C':
      case 'c':
	
	credits();
	
	break;
	
      case 'R':
      case 'r':
	
	listeSauvegardes();
	
	break;
	
      case 127:
      case KEY_DC:
	
	if (_liste.size() > _idListe && remove( (static_cast<string> (DOSSIER_SAUVEGARDES) + static_cast<string> ("/") + _liste[_idListe]).c_str() ) == 0)
	{
	  _liste.erase(_liste.begin()+_idListe);
	}
	else  if (_liste.size() > _idListe)
	{
	  _affichage->ajouterTexteCentreInFrame ("Impossible de supprimer la sauvegarde.", 16, 3, 41);
	  cerr << __FILE__ << " " << __LINE__ << " : le fichier " << (DOSSIER_SAUVEGARDES + '/' + _liste[_idListe]).c_str() << " n'a pas été supprimé correctement " << endl;
	}
	listeSauvegardes();
	
	break;
	
      case KEY_MOUSE:
	
	placementAvecSouris();
	
	break;

      default:
	 cerr << __FILE__ << " " << __LINE__ << " : une touche qui est dans le menu n'a pas été prévue dans le switch !" << endl;
    }
  }
  else if ((_menuM.estDansMenu("Confirmer pseudo") || _menuM.estDansMenu("Confirmer sauvegarde")) && !(*_verrClavier))
  {
    addChar(touche);
  }

  return arret;
}

void Input::lancerNouvellePartie()
{
  // On commence par mettre à jour les actions possibles pour éviter toute erreur
  chargeMenuPartie();

  // On crée une partie vierge
  _score->setNomJoueur(_chaine);
  _score->setTotal(0);
  *_grille = Grille(_score);
  _partie = Partie(_piecesM, _score, _grille);
  _piecesM->selecPiecesDispos();
  _partie.update();

  // On lance l'affichage
  _affichage->viderCentre(); // On commence par nettoyer le centre
  AfficherCadresPartie(); // On affiche le cadre de la grille, les cadres des pièces et le texte sous ces derniers
  MAJAffichagePartie(); // On affiche tous les éléments de la partie
}

void Input::chargePlacementPiece(size_t id)
{
  if (id < 3 && _piecesM->getPiecesDispos()[id] != static_cast<size_t> (-1))
  {
    if (_grille->estPosable((*_piecesM)[_piecesM->getPiecesDispos()[id]]))
    {
      _pieceTenue = id;

      _coorPieceT.y = 5;
      _coorPieceT.x = 5;
      
      afficherPieceGrille();

    }
    else
    {
      _affichage->remplirZone(14, 25, 1, 16, Pixel::WBLACK);
      _affichage->ajouterTexteCentreInFrame ("Plus de place pour cette piece !", 14, 25, 41);
    }
  }
}

void Input::deplacePiece(size_t x, size_t y)
{
  if (y == static_cast<size_t>(-1)) y = 0;
  if (x == static_cast<size_t>(-1)) x = 0;
  
  if (y + (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getHauteur() <= 10 && y < 10)
    _coorPieceT.y = y;
  else
    _coorPieceT.y = 10 - (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getHauteur();

  if (x + (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getLargeur() <= 10 && x < 10)
    _coorPieceT.x = x;
  else
    _coorPieceT.x = 10 - (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getLargeur();

  AffichagePieceT();
}

void Input::placerPiece()
{
  if (_piecesM->poserSurGrille(*_grille, _coorPieceT.x, _coorPieceT.y, _pieceTenue))
  {
    chargeMenuPartie();
    
    _partie.update();
    
    _affichage->remplirZone(14, 25, 10, 16, Pixel::WBLACK);
    MAJAffichagePartie();
  
    if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().premier()) 
      _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(8), 20, 25, 41);
    
    if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().dernier()) 
      _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(9), 21, 25, 41);
    
    _pieceTenue = 4;//Pour éviter de ne plus pouvoir selectioner 0 si on n'a fini sont jeu de pièce avec 0.
    
    if (_piecesM->isGameOver(*_grille)) 
      gameOver();
  }
}

void Input::ecranSaisieNom()
{
  chargeMenuSaisiePseudo();
  _affichage->viderCentre();
  _affichage->ajouterTexteCentreInFrame ("<=( Veuillez saisir votre pseudo )=>", 16, 3, 41);
  _affichage->ajouterTexteCentreInFrame ("<=( Appuyez sur Entree pour valider )=>", 20, 3, 41);
  
  if (_score->getNomJoueur() != "Anonyme")
  {
     _chaine = _score->getNomJoueur();
     _affichage->ajouterTexteCentreInFrame (_chaine, 18, 3, 41);
  }
}

void Input::addChar(int cara)
{
    if (_chaine.length() <= 20 && cara > 31 && cara < 127)
    {
      _chaine += (char)cara;
      _affichage->remplirZone(18, 3, 1, 38, Pixel::WBLACK);
      _affichage->ajouterTexteCentreInFrame (_chaine, 18, 3, 41);
    }
}

void Input::eraseLastChar()
{
  if (_chaine.length() > 0)
  {
    _chaine.pop_back();
    _affichage->remplirZone(18, 3, 1, 38, Pixel::WBLACK);
    _affichage->ajouterTexteCentreInFrame (_chaine, 18, 3, 41);
  }
}

void Input::saisieCoor ()
{
  chargeMenuSaisieCoor();
  
  _affichage->remplirZone(14, 25, 10, 16, Pixel::WBLACK);
  AffichagePieceT();
  _affichage->ajouterTexteCentreInFrame ("Saisissez les coordonnees (1-9)", 16, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 23, 25, 41);
  
  _xModifie = false;
}

void Input::MAJCoor (size_t valeur)
{
  if (valeur < 10)
  {
    if (_xModifie)
    {
	if (valeur + (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getHauteur() <= 10) 
	  deplacePiece(_coorPieceT.x, valeur);
	
	else 
	  deplacePiece(_coorPieceT.x, 10 - (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getHauteur());
    
      afficherPieceGrille();
    }
    else
    {
	if (valeur + (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getLargeur() <= 10) 
	  deplacePiece(valeur, _coorPieceT.y);
	
	else 
	  deplacePiece(10 - (*_piecesM)[_piecesM->getPiecesDispos()[_pieceTenue]].getLargeur(), _coorPieceT.y);
	
	_xModifie = true;
    }
  }
}

void Input::meilleursScores()
{
  _affichage->viderCentre();
  
  std::set<Score::BestScore, std::greater<Score::BestScore>> bestScores = _score->getBestScores();
  
  size_t ligne = 16;
  std::set<Score::BestScore>::const_iterator 
    iteration (bestScores.begin()), 
    fin (bestScores.end()); 
    
  for(;iteration != fin; ++iteration) 
  {
    _affichage->ajouterTexteCentreInFrame (iteration->nom + " " + std::to_string(iteration->score), ligne++, 3, 41);
  }
  
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(1, false), 22, 3, 41);
}

void Input::sortirMeilleursScores ()
{
  chargeMenuPartie();
  
  // On lance l'affichage
  _affichage->viderCentre(); // On commence par nettoyer le centre
  AfficherCadresPartie(); // On affiche le cadre de la grille, les cadres des pièces et le texte sous ces derniers
  MAJAffichagePartie(); // On affiche tous les éléments de la partie
}

void Input::annuler ()
{
  _partie.back();
  
  _affichage->remplirZone(14, 25, 10, 16, Pixel::WBLACK);
  MAJAffichagePartie();

  if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().premier()) 
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(8), 20, 25, 41);
  
  if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().dernier()) 
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(9), 21, 25, 41);
}

void Input::refaire()
{
  _partie.next();
  
  _affichage->remplirZone(14, 25, 10, 16, Pixel::WBLACK);
  MAJAffichagePartie();

  if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().premier()) 
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(8), 20, 25, 41);
  
  if (_partie.getPosHistoricGrille() != _partie.getHistoricGrille().dernier()) 
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(9), 21, 25, 41);
}

void Input::demandeSauvegarde()
{
  chargeMenuDemandeSauvegarde();
  _affichage->viderCentre();
  
  _affichage->ajouterTexteCentreInFrame ("<=( Voulez-vous sauvegarder votre partie ? )=>", 16, 3, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 18, 3, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(3, false), 19, 3, 41);
}

void Input::nomSauvegarde()
{
  chargeMenuNomSauvegarde();
  
  _affichage->viderCentre();
  _affichage->ajouterTexteCentreInFrame ("<=( Veuillez entrer le nom de votre sauvegarde )=>", 16, 3, 41);
  _affichage->ajouterTexteCentreInFrame ("<=( Appuyez sur Entree pour valider )=>", 20, 3, 41);
  
  time_t t = time(NULL);
  char format[100];
  
  strftime(format, sizeof(format), "%Y%m%d_%H%M", localtime(&t));
  
  _chaine = format;
  _affichage->ajouterTexteCentreInFrame (_chaine, 18, 3, 41);
}

void Input::gameOver()
{
  chargeMenuGameOver();
  
  _affichage->afficheASCII("frames/gameover.ascii", Pixel::WBLUE);
  _affichage->ajouterTexteCentreInFrame ("Score : " + to_string( static_cast<int> (_score->getTotal()) ), 19, 3, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 21, 3, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(3, false), 22, 3, 41);
  _affichage->coloration(Pixel::WBLUE);
}

void Input::credits()
{
  chargeMenuCredits();
   _affichage->afficheASCII("frames/credits.ascii", Pixel::WBLUE);
}

void Input::menuPrincipal()
{
  chargeMenuAccueil();
  _affichage->afficheASCII("frames/accueil.ascii", Pixel::WBLUE);
}

void Input::listeSauvegardes()
{
  chargeMenuSauvegarde();
  _affichage->viderCentre();
  _affichage->afficheASCII("frames/saves.ascii", 2, 2, 8, _affichage->getLargeur()-3, Pixel::BGREEN);
  
  // Si la liste des sauvegardes est vide, on cherche des sauvegardes dans la liste des sauvegardes
  if (_liste.size() == 0)
  {
    _idListe = static_cast<size_t> (-1);
  
    DIR* dossier = NULL;
    dossier = opendir(DOSSIER_SAUVEGARDES);

    if (dossier == NULL) 
       _affichage->ajouterTexteCentreInFrame ("Impossible d'ouvrir le dossier des sauvegardes !", 19, 3, 41);
    
    else
    {
      struct dirent* fichier = NULL;

      while ((fichier = readdir(dossier)) != NULL)
      {
	if (static_cast<string>(fichier->d_name).find(".save") != string::npos)
	  _liste.push_back(fichier->d_name);
      }
      
      if (_liste.size() > 0)
      {
	_idListe = 0;
	 sort(_liste.begin(), _liste.end(), greater<string>());
      }

      closedir(dossier);
    }
  }
  
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 12, 3, 41); // Echap = retourner au menu principal
  
  // Si il n'y a aucun fichier
  if (_liste.size() == 0)
    _affichage->ajouterTexteCentreInFrame ("Aucune sauvegarde.", 19, 3, 41);
  
  else 
  {
    if (_idListe >= _liste.size())
      _idListe = _liste.size() -1;
    
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(1, false), 13, 3, 41);
    _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(4, false), 14, 3, 41);
    
    if (_idListe < _liste.size() -1) 
      _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(6, false), 17, 3, 41);
    if (_idListe > 0) 
      _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(7, false), 18, 3, 41);
    
    for (size_t i = _idListe; i < 15 + _idListe && i < _liste.size(); ++i)
    {
      if (i == _idListe) 
	_affichage->ajouterTexteCentreInFrame (" => " + _liste[i].substr(0,_liste[i].size()-5) + " <= ", 20 + i - _idListe, 3, 41);
      else
	_affichage->ajouterTexteCentreInFrame (_liste[i].substr(0,_liste[i].size()-5), 20 + i - _idListe, 3, 41);
    }
  }
  
}

void Input::chargeSauvegarde()
{
  chargeMenuPartie();
  
  _partie = Partie(_piecesM, _score, _grille);
  _partie.load(static_cast<string> (DOSSIER_SAUVEGARDES) + static_cast<string> ("/") + _liste[_idListe]);
  
  // On lance l'affichage
  _affichage->viderCentre(); // On commence par nettoyer le centre
  AfficherCadresPartie(); // On affiche le cadre de la grille, les cadres des pièces et le texte sous ces derniers
  MAJAffichagePartie(); // On affiche tous les éléments de la partie
  
}

void Input::placementAvecSouris()
{
  MEVENT event;
  int bouton;
  
  if(getmouse(&event) == OK) 
  {
    _posSouris.x = event.x - _affichage->getPositionFrame().x;
    _posSouris.y = event.y - _affichage->getPositionFrame().y;
    bouton = event.bstate;
    
    if(bouton & BUTTON1_CLICKED)
    {
      if (_posSouris.y >= 26 && _posSouris.y <= 34)
      {
	if (_posSouris.x >= 8 && _posSouris.x <= 24 && _menuM.estDansMenu("Piece 1") && _pieceTenue != 0)
	  chargePlacementPiece(0);
	
	else if (_posSouris.x >= 36 && _posSouris.x <= 50 && _menuM.estDansMenu("Piece 2") && _pieceTenue != 1)
	  chargePlacementPiece(1);
	
	else if (_posSouris.x >= 62 && _posSouris.x <= 76 && _menuM.estDansMenu("Piece 3") && _pieceTenue != 2)
	  chargePlacementPiece(2);
      }
      if (_menuM.estDansMenu("Souris Placement") && _posSouris.y >= 3 && _posSouris.y <= 23 && _posSouris.x >= 6 && _posSouris.x <= 46)
      {
	deplacePiece( (_posSouris.x-6) / 4, (_posSouris.y-3) / 2);
	placerPiece();
      }
    }
    
    if (_menuM.estDansMenu("Souris Placement") && _posSouris.y >= 3 && _posSouris.y <= 23 && _posSouris.x >= 6 && _posSouris.x <= 46)
    {
      deplacePiece( (_posSouris.x-6) / 4, (_posSouris.y-3) / 2);
    }
  }
}

void Input::AffichagePieceT()
{
  MAJAffichagePartie();
  _affichage->viderCadrePiece (_pieceTenue);
  _affichage->afficherPieceGrille (*_piecesM, _pieceTenue, _coorPieceT);
  _affichage->remplirZone(14, 25, 1, 16, Pixel::WBLACK);
  string texte = "Coordonnees : ("; texte += to_string(static_cast<int> (_coorPieceT.x)); texte += ", "; texte += to_string(static_cast<int> (_coorPieceT.y)); texte += ")";
  _affichage->ajouterTexteCentreInFrame (texte, 14, 25, 41);
}

void Input::AfficherCadresPartie()
{
  _affichage->afficheASCII("frames/1010.ascii", 2, 25, 8, _affichage->getLargeur()-3, Pixel::BYELLOW);
  _affichage->tracerCadreGrille();
  _affichage->tracerCadresPieces();
  _affichage->afficherTexteCadres();
}

void Input::MAJAffichagePartie()
{
  _affichage->afficherGrille(*_grille);
  _affichage->afficherScore(*_score);

  _affichage->remplirCadresPieces();
  _affichage->afficherTexteCadres();
  _affichage->afficherPiecesDispos(*_piecesM);
  
  _affichage->remplirZone(22, 25, 2, 16, Pixel::WBLACK);
  
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(6), 22, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 23, 25, 41);
}

void Input::afficherPieceGrille()
{
  chargeMenuPlacement();
  
  _affichage->remplirZone(14, 25, 10, 16, Pixel::WBLACK);
  AffichagePieceT();
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(1), 17, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(2), 18, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(3), 19, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(4), 20, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(13), 21, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(7), 22, 25, 41);
  _affichage->ajouterTexteCentreInFrame (_menuM.descMenu(0, false), 23, 25, 41);
}