#include "option.h"
#include "piecesManager.h"

/*--------------------
 *	CONSTRUCTORS
 */

Option::Option () {}

Option::Option (size_t id, const std::string& name, const std::string& shortcut, const std::string& description , Option::Type type) : 
  _name(name), 
  _shortcut(shortcut), 
  _description(description), 
  _type(type), 
  _callbackVoid(NULL),
  _callbackPieMan(NULL),
  _callbackString(NULL)
  {}

Option::Option (size_t id, const std::string& name, const std::string& shortcut, const std::string& description , Option::Type type, Option::CallbackFuncVoid callback) : 
  _name(name), 
  _shortcut(shortcut), 
  _description(description),  
  _type(type), 
  _callbackVoid(callback),
  _callbackPieMan(NULL),
  _callbackString(NULL)
  {}

Option::Option (size_t id, const std::string& name, const std::string& shortcut, const std::string& description , Option::Type type, Option::CallbackFuncPieMan callback, PiecesManager& pieMan) : 
  _name(name), 
  _shortcut(shortcut), 
  _description(description),  
  _type(type), 
  _callbackVoid(NULL),
  _callbackPieMan(callback),
  _callbackString(NULL),
  _piecesManager(&pieMan)
  {}

Option::Option (size_t id, const std::string& name, const std::string& shortcut, const std::string& description , Option::Type type, Option::CallbackFuncString callback, const std::string& chaine) : 
  _name(name), 
  _shortcut(shortcut), 
  _description(description),  
  _type(type), 
  _callbackVoid(NULL),
  _callbackPieMan(NULL),
  _callbackString(callback),
  _chaine(chaine)
  {}


/*-----------------
 *	GETTERS
 */

size_t Option::getId() const  { return _id; }
const std::string& Option::getName() const { return _name; }
const std::string& Option::getShortcut() const { return _shortcut; }
const std::string& Option::getDescription() const { return _description; }
Option::Type Option::getType() const { return _type; }

const std::string& Option::getChaine() const { return _chaine; }
PiecesManager* Option::getPiecesManager() const { return _piecesManager; }

bool Option::hasCallback() const { return _callbackVoid != NULL || _callbackPieMan != NULL || _callbackString != NULL; }
bool Option::launchCallback() const 
{ 
	return _callbackVoid != NULL ? _callbackVoid() :
	       _callbackPieMan != NULL ? _callbackPieMan(_chaine, _piecesManager) :
	       _callbackString != NULL ? _callbackString(_chaine) :
	       false;
}


/*---------------
 *	SETTERS
 */

void Option::setId (size_t id)
{
	_id = id;
}

void Option::setName (const std::string& name)
{
	_name = name;
}

void Option::setShortcut (const std::string& shortcut)
{
	_shortcut = shortcut;
}

void Option::setDescription (const std::string& description)
{
	_description = description;
}

void Option::setChaine (const std::string& chaine)
{
	_chaine = chaine;
}

void Option::setChaine (const char* chaine)
{
	_chaine = chaine;
}

void Option::setPiecesManager (PiecesManager& pieMan)
{
	_piecesManager = &pieMan;
}

void Option::setCallbackVoid (CallbackFuncVoid callback)
{
	_callbackVoid = callback;
}

void Option::setCallbackPieMan (CallbackFuncPieMan callback)
{
	_callbackPieMan = callback;
}

void Option::setCallbackString (CallbackFuncString callback)
{
	_callbackString = callback;
}

std::string Option::Type2String(Option::Type t) 
{
	std::string tmp;
	switch (t) 
	{
		case Option::AUCUN:    tmp = ""; break;
		case Option::BOOLEEN:  tmp = "<booléen>"; break;
		case Option::ENTIER:   tmp = "<entier>"; break;
		case Option::REEL:     tmp = "<réel>"; break;
		case Option::CHAR:     tmp = "<caractère>"; break;
		case Option::CHAINE:   tmp = "<chaîne>"; break;
	}
	return tmp;
}

void Option::print() const 
{
	std::cout << _name << " (" << _shortcut << ") " << Type2String(_type)
	    << "\t" << _description << std::endl;
}

std::ostream& Option::affiche (std::ostream& os) const
{
	return os << affiche();
}

std::string Option::affiche () const
{
	std::string tmp;
	tmp =  _name + " (" + _shortcut + ") " + Type2String(_type)
	    + "\t" + _description + "\n";
	return tmp;
}

std::ostream& operator<< (std::ostream& os, const Option& opt)
{
	return opt.affiche(os);
}