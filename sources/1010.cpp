/**
 * \file 1010.cpp
 * \authors Guillaume Pérution-Kihli, Desgenetez Clement, Judicael Russo, Johan Guillen
 * Contient la fonction principale du jeu
 */

#include <fstream>
#include <chrono>
#include <thread>
#include <curses.h>
#include "listOption.h"
#include "affichage.h"
#include "score.h"
#include "grille.h"
#include "piecesManager.h"
#include "input.h"

#define VERSION 1

using namespace std;

// Callbacks des différentes options
bool OptionAuteur () { cout << "Auteurs: Guillaume Pérution-Kihli, Desgenetez Clement, Judicael Russo, Johan Guillen" << endl; return true; }
bool OptionHelp (string chaine) { cout << chaine << endl; return true; }
bool OptionVersion () { cout << "Version : " << VERSION; return true; }
bool OptionAffiche (string nomFichier) { ifstream entree(nomFichier, ios::in); string ligne; while(getline(entree, ligne)) cout << ligne << endl; return true; }
bool OptionConf (string nomFichier, PiecesManager* piecesManager) { piecesManager->loadConf(nomFichier); return false; }

int main (int argc, char** argv, char** env)
{
  ofstream cerrFlux("logs/cerr.txt", std::ofstream::out);
  streambuf* cerrPrecBuf = cerr.rdbuf(cerrFlux.rdbuf());
  
  // On instancie d'ores et déjà le PiecesManager car il est nécessaire pour charger la conf si une option le demande
  PiecesManager piecesManager;

  /*
   * On charge la liste des options
   */
  
  listOption list;

  list.addOption(Option(0, "--authors", "-a", "\tAffiche les auteurs", Option::AUCUN, OptionAuteur));
  list.addOption(Option(1, "--version", "-v", "\tAffiche la version du programme", Option::AUCUN, OptionVersion));
  list.addOption(Option(2, "--pieces", "-p", "Charge un autre jeu de pièces", Option::CHAINE, OptionConf, piecesManager));
  list.addOption(Option(3, "--conf", "-c", "Charge un autre jeu de pièces", Option::CHAINE, OptionConf, piecesManager));
  list.addOption(Option(4, "--help", "-h", "\tAffiche l'aide du programme", Option::AUCUN));
  list.addOption(Option(5, "--IWantACake", "-i", "Pour les petites faims", Option::AUCUN, OptionAffiche, "frames/tcial.ascii"));

  Option* optionHelp;
  optionHelp = &list[list.getOptionByNoS("--help")];
  optionHelp->setCallbackString(OptionHelp);
  optionHelp->setChaine(list.usage(argv[0]));

  // On parse les options et on vérifie si l'une des options nécessite que l'on n'exécute pas la suite du programme
  if (list.parseOptions(argc, argv))
  {
    // On stoppe les redirection de cerr vers le fichier de log
    cerr.rdbuf(cerrPrecBuf);
    cerrFlux.close();
    
    return 0; // sortie de la fonction pour fermer le programme
  }

  /*
   * On instancie les objets nécessaires au fonctionnement du programme
   */
  
  bool verrClavier = false;
  Affichage affichage(&verrClavier);
  Score score;
  Grille grille (&score);
  Input input(&grille, &score, &piecesManager, &affichage, &verrClavier);
  
  string texte;
  bool arret = false;
  
  /*
   * On lance la boucle qui fait fonctionner le programme
   * Elle est limitée à 1000 itérations par seconde afin d'éviter d'utiliser le processeur pour rien
   */

  while(!arret)
  {
    // On vérifie que la taille du terminal est assez grand pour afficher correctement le jeu
    affichage.checkDimensions();
    
    // On capture la touche pour exécuter les actions en conséquence, et on vérifie si le programme ne doit pas être arrêté
    arret = input.catchKey(getch());
    
    // On attend 1 milliseconde afin que la boucle ne tourne pas à 1.000.000 d'itérations par seconde
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
  }

  // On stoppe les redirection de cerr vers le fichier de log
  cerr.rdbuf(cerrPrecBuf);
  cerrFlux.close();

  return 0;
}
