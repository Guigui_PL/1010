
template <typename Type>
void ListeDC<Type>::setSucc (ListeDC<Type>::Element e, ListeDC<Type>::Element succ) { e->setSucc(succ); }

template <typename Type>
void ListeDC<Type>::setPred (ListeDC<Type>::Element e, ListeDC<Type>::Element pred) { e->setPred(pred); }

template <typename Type>
ListeDC<Type>::ListeDC() : _tete(NULL), _queue(NULL), _taille(0) {}

template <typename Type>
ListeDC<Type>::ListeDC(const ListeDC<Type> &LDC) : _tete(NULL), _queue(NULL), _taille(0)
{
  Element e = LDC.premier();
  while (e != NULL)
  {
    ajoutFin (LDC.info(e));
    e = LDC.succ(e);
  }
}

template <typename Type>
ListeDC<Type>::~ListeDC() { vider(); }

template <typename Type>
ListeDC<Type>& ListeDC<Type>::operator= (const ListeDC<Type> &LDC)
{
  if (this != &LDC)
  {
    this->vider();
    
    Element e = LDC.premier();
    while (e != NULL)
    {
      ajoutFin (LDC.info(e));
      e = LDC.succ(e);
    }
  }
  return *this;
}

template <typename Type>
bool ListeDC<Type>::estVide() const { return _tete == NULL; }

template <typename Type>
size_t ListeDC<Type>::getTaille() const { return _taille; }

template <typename Type>
typename ListeDC<Type>::Element ListeDC<Type>::premier () const { return _tete; }

template <typename Type>
typename ListeDC<Type>::Element ListeDC<Type>::dernier () const { return _queue; }

template <typename Type>
typename ListeDC<Type>::Element ListeDC<Type>::succ (ListeDC<Type>::Element e) const { return e->getSucc(); }

template <typename Type>
typename ListeDC<Type>::Element ListeDC<Type>::pred (ListeDC<Type>::Element e) const { return e->getPred(); }

template <typename Type>
const Type& ListeDC<Type>::info (ListeDC<Type>::Element e) const { return e->getInfo(); }

template <typename Type>
void ListeDC<Type>::ajoutAvant (ListeDC<Type>::Element e, const Type& val)
{
  Cell<Type> *cellule = new Cell<Type>(val);
  _taille++;
  
  if (e == NULL)
  {
    _tete = _queue = cellule;
  }
  else if (e == _tete)
  {
    setSucc(cellule, e);
    setPred(e, cellule);
    _tete = cellule;
  }
  else
  {
    setSucc(cellule, e);
    setPred(cellule, pred(e));
    setSucc(pred(e), cellule);
    setPred(e, cellule);
  }
}

template <typename Type>
void ListeDC<Type>::ajoutApres (ListeDC<Type>::Element e, const Type& val)
{
  Cell<Type> *cellule = new Cell<Type>(val);
  _taille++;
  
  if (e == NULL)
  {
    _tete = _queue = cellule;
  }
  else if (e == _queue)
  {
    setPred(cellule, e);
    setSucc(e, cellule);
    _queue = cellule;
  }
  else
  {
    setPred(cellule, e);
    setSucc(cellule, succ(e));
    setPred(succ(e), cellule);
    setSucc(e, cellule);
  }
}

template <typename Type>
void ListeDC<Type>::ajoutDebut (const Type& val) { ajoutAvant(_tete, val); }

template <typename Type>
void ListeDC<Type>::ajoutFin (const Type& val) { ajoutApres(_queue, val); }

template <typename Type>
void ListeDC<Type>::vider ()
{
  Element e = _tete;
  while (e != NULL)
  {
    e = succ(e);
    delete _tete;
    _tete = e;
  }
  _tete = _queue = NULL;
  _taille = 0;
}

template <typename Type>
void ListeDC<Type>::supprimer (ListeDC<Type>::Element e)
{
  if (e == NULL) 
    return;
    
  if (_taille == 1)
  {
    _tete = _queue = NULL;
  }
  else if (e == _tete)
  {
    _tete = succ(e);
    setPred(_tete, NULL);
  }
  else if (e == _queue)
  {
    _queue = pred(e);
    setSucc(_queue, NULL);
  }
  else 
  {
    setSucc(pred(e), succ(e));
    setPred(succ(e), pred(e));
  }
  _taille--;
  delete e;
}

template <typename Type>
void ListeDC<Type>::supprItv (ListeDC<Type>::Element deb, ListeDC<Type>::Element fin)
{
  Element aux;
  while (deb != fin && deb != NULL)
  {
    aux = succ(deb);
    supprimer(deb);
    deb = aux;
  }
  
  if (fin != NULL) supprimer(fin);
}