#ifndef MENU_H
#define MENU_H

#include <string>

class Menu
{
	
	private:
		size_t _position;
		std::string _name;
		int _touche;
		std::string _description;
	public:
		Menu();
		Menu(size_t, const std::string&, int, const std::string&);

		size_t getPosition() const;
		const std::string& getName() const;
		int getTouche() const;
		const std::string& getDescription() const;

		void setPosition (size_t);
		void setName (const std::string&);
		void setTouche (int);
		void setDescription (const std::string&);
};

#endif

