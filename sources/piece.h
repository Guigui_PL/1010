#ifndef PIECE_H
#define PIECE_H
#include "matriceCases.h"

class Piece : public MatriceCases
{
  
public:
  enum Probabilite
  {
    NULLE,
    FAIBLE,
    MOYENMT_FAIBLE,
    MOYENNE,
    MOYENMT_FORTE,
    FORTE
  };
  
private:
  Probabilite _probabilite;
  
public:
  Piece ();
  Piece(size_t, size_t, Probabilite);
  Piece(const Piece&);
  
  Piece& operator=(const Piece&);
  
  void setProbabilite(Probabilite);
  
  Probabilite getProbabilite() const;
  
  /**
   * \fn print() const
   * Affiche le contenu de la pièce dans cerr dans un but de débogage
   */
  
  void print() const;
  
};

#endif
