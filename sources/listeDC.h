#ifndef LISTEDC_H
#define LISTEDC_H
#include "cell.h"

template <typename Type>
class ListeDC 
{
public:
  typedef Cell<Type>* Element;
  
private:
  Element _tete;
  Element _queue;
  size_t _taille;
  
  void setSucc (Element, Element);
  void setPred (Element, Element);
  
public:
  ListeDC();
  ListeDC(const ListeDC<Type>&);
  ~ListeDC();
  
  ListeDC<Type>& operator= (const ListeDC<Type>&);
  
  bool estVide() const;
  size_t getTaille() const;
  
  Element premier () const;
  Element dernier () const;
  
  Element succ (Element) const;
  Element pred (Element) const;
  const Type& info (Element) const;
  
  void ajoutAvant (Element, const Type&);
  void ajoutApres (Element, const Type&);
  
  void ajoutDebut (const Type&);
  void ajoutFin (const Type&);
  
  void vider ();
  void supprimer (Element);
  void supprItv (Element, Element);
};

#include "listeDC.tpp"

#endif
