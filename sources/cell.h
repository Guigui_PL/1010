#ifndef CELL_H
#define CELL_H

template <typename Type>
class Cell 
{
private:
  Type _info;
  Cell<Type>* _succ;
  Cell<Type>* _pred;
  
public:
  Cell();
  Cell(const Type& info);
  Cell(const Type& info, Cell<Type>* succ, Cell<Type>* pred);
  ~Cell();
  
  const Type& getInfo() const;
  Cell<Type>* getPred() const;
  Cell<Type>* getSucc() const;
  
  void setInfo(const Type& info);
  void setPred(Cell<Type>* pred);
  void setSucc(Cell<Type>* succ);
  
};

#include "cell.tpp"

#endif