#ifndef OPTION_H
#define OPTION_H
#include <string>
#include <iostream>

// déclaration anticipée 
class PiecesManager;

class Option
{
      public:
	enum Type {
		AUCUN,
		BOOLEEN,
		ENTIER,
		REEL,
		CHAR,
		CHAINE
	};

	/* On définit les différents formats de pointeurs vers des fonctions pour les callbacks des options */
	typedef bool (*CallbackFuncVoid) ();
	typedef bool (*CallbackFuncPieMan) (std::string, PiecesManager*);
	typedef bool (*CallbackFuncString) (std::string);
  
	private:
		/* Attributs "classiques" des options */
		size_t _id;
		std::string _name;
		std::string _shortcut;
		std::string _description;
		Type _type;
		
		/* Attributs servant aux callbacks des différentes options
		 * On commence par les pointeurs vers les fonctions */
		CallbackFuncVoid _callbackVoid;
		CallbackFuncPieMan _callbackPieMan;
		CallbackFuncString _callbackString;
		
		/* Puis on crée les attributs stockant les éventuels paramètres */
		std::string _chaine;
		PiecesManager* _piecesManager;
		

	public:
		/* Getters des attributs "classiques" */
		size_t getId() const  ;
		const std::string& getName() const ;
		const std::string& getShortcut() const ;
		const std::string& getDescription() const ;
		Type getType() const;
		
		/* Getters des éventuels paramètres des callbacks */
		const std::string& getChaine() const;
		PiecesManager* getPiecesManager() const;
		
		/* Méthodes constantes utiles aux callbacks */
		bool hasCallback() const;
		bool launchCallback() const;

		/* Les différents constructeurs
		 * Les callbacks doivent oblgiatoirement être indiqués lors de la construction de l'option */
		Option();
		Option(size_t, const std::string&, const std::string&, const std::string&, Type);
		Option(size_t, const std::string&, const std::string&, const std::string&, Type, CallbackFuncVoid);
		Option(size_t, const std::string&, const std::string&, const std::string&, Type, CallbackFuncPieMan, PiecesManager&);
		Option(size_t,const  std::string&, const std::string&, const std::string&, Type, CallbackFuncString, const std::string&);

		/* Setters des attributs */
		void setId (size_t);
		void setName (const std::string&);
		void setShortcut (const std::string&);
		void setDescription (const std::string&);
		
		void setChaine (const std::string&);
		void setChaine (const char*);
		void setPiecesManager (PiecesManager&);
		
		void setCallbackVoid (CallbackFuncVoid);
		void setCallbackPieMan (CallbackFuncPieMan);
		void setCallbackString (CallbackFuncString);

		/* Fonctions d'affichage */
		void print() const;
		std::ostream& affiche (std::ostream&) const;
		std::string affiche () const;
		
		/* Conversion de l'énumération "Type" en chaîne de caractères */
		static std::string Type2String(Type t);
};

/* Surcharge de l'opérateur << pour afficher le contenu de l'option */
std::ostream& operator<< (std::ostream&, const Option&);

#endif
