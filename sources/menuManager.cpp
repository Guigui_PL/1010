#include "menuManager.h"
#include <iostream>
#include <ncurses.h>


MenuManager::MenuManager()
{
  initIntToKey();
}
  
void MenuManager::initIntToKey()
{
  intToKey[10] = "Entree";
  intToKey[13] = "Entree";
  intToKey[KEY_ENTER] = "Entree";
  intToKey['&'] = "1";
  intToKey['"'] = "3";
  intToKey[KEY_BACKSPACE] = "Retour";
  intToKey[27] = "Echap";
  intToKey[KEY_UP] = "^";
  intToKey[KEY_DOWN] = "v";
  intToKey[KEY_LEFT] = "<";
  intToKey[KEY_RIGHT] = ">";
  intToKey[127] = "Suppr";
  intToKey[KEY_DC] = "Suppr";
}

std::string MenuManager::descMenu(size_t id, bool descCourte)
{
  if (id >= _listMenu.size()) return "";
  
  std::string touche;
  
  if (intToKey.find(_listMenu[id].getTouche()) != intToKey.end()) 
    touche = intToKey[_listMenu[id].getTouche()];
  else
    touche = (char)_listMenu[id].getTouche();
  
  if (descCourte)
    return touche + " - " + _listMenu[id].getName();
  
  return touche + " - " + _listMenu[id].getDescription();
}

/*
 *Do : Return the Option corresponding to the pram Name or Touche
 *@param STRING
 *@return INTEGER
 */
size_t MenuManager::getOptionMenuByNoS(int _nos) const
{
	bool found = false;
	size_t i = 0;

	while( !found && (i < _listMenu.size()))
	{
		found = _listMenu[i].getTouche() == _nos;
		i++;
	}

		return found ? (i-1) :static_cast<size_t> (-1) ;
} 

/*
 * DO : Add an option to the _listMenu
 * @param   &MENU
 * @return VOID
 */
void MenuManager::addOptionMenu(const Menu &menu)
{
	bool exist = (getOptionMenuByNoS(menu.getTouche()) !=static_cast<size_t> (-1));

	if(exist)
		std::cerr << "Warning : La position " << menu.getPosition() << " est déjà utilisée. " << std::endl;

	else
		_listMenu.push_back(menu);
}

size_t MenuManager::getNbOptions() const
{
  return _listMenu.size();
}

const Menu& MenuManager::getMenu(size_t id) const
{
  if (id >= _listMenu.size())
  {
    std::cerr << "Warning : La position " << id << " n'existe pas. " << std::endl;
    std::terminate();
  }
  return _listMenu[id];
}

bool MenuManager::estDansMenu(int touche) const
{
  if (_listMenu.size() == 0) return false;
  
  bool estDedans = false;
  size_t i = 0;
  do
  {
    
    if (_listMenu[i].getTouche() == touche) 
      estDedans = true;
      
  } while (!estDedans && ++i < _listMenu.size());
  
  return estDedans;
}

bool MenuManager::estDansMenu(const std::string& name) const
{
  if (_listMenu.size() == 0) return false;
  
  bool estDedans = false;
  size_t i = 0;
  do
  {
    
    if (_listMenu[i].getName() == name || _listMenu[i].getDescription() == name) 
      estDedans = true;
      
  } while (!estDedans && ++i < _listMenu.size());
  
  return estDedans;
}