#ifndef PIXEL_H
#define PIXEL_H
#include <string>
#include <vector>

class Pixel 
{
public:
  enum Color {
    WBLACK = 1,  // couleur fond = noir ,   couleur texte = blanc
    WCYAN,   // couleur fond = cyan,    couleur texte = blanc
    WBLUE,   // couleur fond = bleu,    couleur texte = blanc
    WYELLOW, // couleur fond = jaune,   couleur texte = blanc
    WGREEN,  // couleur fond = vert,    couleur texte = blanc
    WMAGENTA,// couleur fond = magenta, couleur texte = blanc
    WRED,	   // couleur fond = rouge,   couleur texte = blanc
    BWHITE,  // couleur fond = blanc,   couleur texte = blanc
    BCYAN,   // couleur fond = cyan,    couleur texte = noir
    BBLUE,   // couleur fond = bleu,    couleur texte = noir
    BYELLOW, // couleur fond = jaune,   couleur texte = noir
    BGREEN,  // couleur fond = vert,    couleur texte = noir 
    BMAGENTA,// couleur fond = magenta, couleur texte = noir
    BRED,    // couleur fond = rouge,   couleur texte = noir
    RBLACK,  // couleur fond = noir ,   couleur texte = rouge
    GBLACK   // couleur fond = noir ,   couleur texte = vert
  };
  
private:
  char _caractere1;
  char _caractere2;
  Color _couleur;
  
public:
  Pixel();
  Pixel(Color);
  Pixel(Color, char);
  Pixel(Color, char, char);
  
  static std::vector<Pixel> texteInPixels (const std::string&, Pixel::Color);
  
  void setCaracteres(char, char);
  void setCouleur(Color);
  
  char getCaractere1() const;
  char getCaractere2() const;
  std::string getCaracteres() const;
  Color getCouleur() const;
};

#endif