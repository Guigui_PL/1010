#ifndef INPUT_H
#define INPUT_H
#include <string>
#include "partie.h"
#include "menuManager.h"
#include "affichage.h"

// déclarations anticipées
class Score;
class PiecesManager;
class Grille;

class Input
{
 private:
	MenuManager _menuM;
	Partie _partie ;
	PiecesManager* _piecesM;
	Grille* _grille;
	Score* _score;
	Affichage* _affichage;
	bool* _verrClavier;
	
	// Attributs concernant la pièce qui a été prise
	size_t _pieceTenue;
	Affichage::Coor _coorPieceT;
	
	// Attribut permettant de stocker une chaîne de caractère (pseudo ou nom de sauvegarde)
	std::string _chaine;
	
	// Savoir si la coordonnée x a été modifiée lors de la saisie des coordonnées d'une pièce
	bool _xModifie;
	
	// Retenir la liste des sauvegardes lors que l'on veut reprendre une partie sauvegardée
	std::vector<std::string> _liste;
	size_t _idListe;
	
	// Gestion de la souris
	Affichage::Coor _posSouris;

 public:
	/*
	 * Constructeurs
	 */
	
	Input();
	Input(Grille* grille, Score* score, PiecesManager* piecesM, Affichage* affichage, bool* verrClavier);
	
	/*
	 * Accesseurs
	 */
	
	const MenuManager& getMenuM() const;
	const Partie& getPartie() const;
	PiecesManager* getPiecesM() const;
	Score* getScore() const;
	Affichage* getAffichage()const;
	
	/*
	 * Mutateurs
	 */
	
	void setMenuM(const MenuManager&);
	void setPartie(const Partie&);
	void setPiecesM(PiecesManager*);
	void setPiecesM(PiecesManager&);
	void setScore(Score*);
	void setScore(Score&);
	void setAffichage(Affichage*);
	void setAffichage(Affichage&);
	
	/**
	 * \fn catchKey()
	 * \brief reçoit la touche appuyée et effectue les actions en conséquence
	 * \param int touche : numéro de la touche appuyée
	 */
	
	bool catchKey(int touche);
	
	/*
	 * Les différents menus
	 */
	
	void chargeMenuAccueil();
	void chargeMenuPartie();
	void chargeMenuPlacement();
	void chargeMenuSaisiePseudo();
	void chargeMenuSaisieCoor();
	void chargeMenuBestScoresPartie();
	void chargeMenuBestScoresGameOver();
	void chargeMenuDemandeSauvegarde();
	void chargeMenuNomSauvegarde();
	void chargeMenuGameOver();
	void chargeMenuCredits();
	void chargeMenuSauvegarde();
	
	/*
	 * Fonctions effectuant les différentes actions
	 */
	
	void lancerNouvellePartie();
	
	void chargePlacementPiece(size_t id);
	void deplacePiece(size_t x, size_t y);
	void placerPiece();
	
	void ecranSaisieNom();
	void addChar(int cara);
	void eraseLastChar();
	
	void saisieCoor();
	void MAJCoor(size_t valeur);
	
	void meilleursScores();
	void sortirMeilleursScores();
	
	void annuler();
	void refaire();
	
	void demandeSauvegarde();
	void nomSauvegarde();
	
	void gameOver();
	
	void menuPrincipal();
	
	void credits();
	
	void listeSauvegardes();
	void chargeSauvegarde();
	
	void placementAvecSouris();
	
	/*
	 * Méthodes pour l'affichage
	 */
	
	void AfficherCadresPartie();
	void MAJAffichagePartie();
	void AffichagePieceT();
	void afficherPieceGrille();


};
#endif
