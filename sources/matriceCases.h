#ifndef MATRICECASES_H 
#define MATRICECASES_H
#include "case.h"

class MatriceCases 
{
protected:
  Case** _cases;
  size_t _largeur;
  size_t _hauteur;
  
public:
  MatriceCases();
  MatriceCases(size_t, size_t);
  MatriceCases(const MatriceCases&);
  virtual ~MatriceCases();
  
  MatriceCases& operator=(const MatriceCases&);
  Case* operator[](size_t);
  const Case* operator[](size_t) const;
  
  void alloc();
  void desalloc();
  
  void setCase(size_t, size_t, const Case&);
  void setHauteur(size_t);
  void setLargeur(size_t);
  
  const Case& getCase(size_t, size_t) const;
  size_t getLargeur() const;
  size_t getHauteur() const;
};

#endif
