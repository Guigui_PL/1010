#ifndef CASE_H
#define CASE_H
#include "pixel.h"

class Case
{
  public:
    enum Couleur
    {
      BLANC,
      NOIR,
      BLEU,
      JAUNE,
      VERT,
      ROUGE
    };

  private:
    bool _vide;
    Couleur _couleur;
    
  public:
    Case();
    Case(bool vide, Couleur couleur);
    
    bool estVide() const;
    Couleur getCouleur() const;
    Pixel::Color getCouleurPixel() const;
    
    void setCouleur(Couleur couleur);
    void setVide(bool vide);
};

#endif