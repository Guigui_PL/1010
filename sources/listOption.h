#ifndef LISTOPTION_H
#define LISTOPTION_H

#include <iostream>
#include <string>
#include "option.h"

#define MAX_OPTION 10

class listOption{

	private:
		Option _list[MAX_OPTION];
		size_t _nb_options;

	public:
		/* Constructeur */
		listOption();
		
		Option& operator[](size_t);
		const Option& operator[](size_t) const;
		
		/* Getters */
		size_t getOptionByNoS(const std::string&) const;
		size_t getNbOptions () const;
		const Option& getOption(size_t) const;
		
		bool optionHasArgument(const std::string &opt) const;
		Option::Type optionArgumentType(const std::string &opt) const;
		
		void addOption(const Option &option);
		
		/* Fonctions d'affichage des options */
		void print() const;
		std::string affiche () const;
		std::string usage (char*) const;
		
		/* Parsage des options et lancement des callbacks en conséquence */
		bool parseOptions(int argc, char** argv);

};

/* Surcharge de l'opérateur << pour afficher le contenu des options */
std::ostream& operator<< (std::ostream&, const listOption&);

#endif
