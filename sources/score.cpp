#include "score.h"
#include <iostream>
#include <fstream>

bool Score::BestScore::operator< (const Score::BestScore& best) const
{
  return score < best.score;
}

bool Score::BestScore::operator> (const Score::BestScore& best) const
{
  return score > best.score;
}
    
size_t Score::tblScores[7] = {0, 10, 25, 45, 60, 75, 90};

Score::Score() : total(0), meilleur(0), nomJoueur("Anonyme") 
{
  chargeBestScores();
  chargeBestScorePerso();
}

Score::Score(const std::string& nom) : total(0)
{
  chargeBestScores();
  chargeBestScorePerso();
  setNomJoueur(nom);
}

Score::Score(size_t to, size_t me, const std::string& nom):total(to), meilleur(me)
{
  chargeBestScorePerso();
  setNomJoueur(nom);
}

Score::~Score()
{
  ajouterBest(nomJoueur, total);
  saveBestScorePerso();
}

void Score::ajouterBest(std::string nom, size_t score)
{
  bestScores.insert({nom, score});
  
  if (bestScores.size() > 5)
    bestScores.erase(--bestScores.end());
  
  saveBestScores();
}

//Get
size_t Score::getTotal() const
{
    return total;
}

size_t Score::getMeilleur() const
{
    return meilleur;
}

const std::string& Score::getNomJoueur() const
{
    return nomJoueur;
}

const std::set<Score::BestScore, std::greater<Score::BestScore>>& Score::getBestScores() const
{
    return bestScores;
}

//Set
void Score::setTotal(size_t to)
{
    total = to;
    if(total > meilleur)
        setMeilleur(total);
}

void Score::setMeilleur(size_t me)
{
    meilleur = me;
}

void Score::setNomJoueur(std::string nom)
{
  bool estVide = true;
  for (size_t i = 0; i < nom.length(); ++i)
    if (nom[i] != ' ') estVide = false;
    
  if (nom.length() > 20)
    nomJoueur = nom.substr(0,19);
  else if (estVide)
    nomJoueur = "Anonyme";
  else
    nomJoueur = nom;
}

void Score::updateScore(size_t col, size_t lig)
{
  if (col > 0 && lig > 0)
    setTotal(total + (tblScores[col] + tblScores[lig]) * 2);
  else
    setTotal(total + tblScores[col] + tblScores[lig]);
}


void Score::chargeBestScores()
{
  std::string nom;
  size_t score;
  std::ifstream scoreFile("saves/bestScores.save", std::ios::in);
  
  if (scoreFile.is_open())
  {
    while (!scoreFile.eof())
    {
      scoreFile >> nom >> score;
      ajouterBest(nom, score);
    }
    scoreFile.close();
  }
  else 
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : le fichier des meilleurs scores n'a pu �tre ouvert ! " << std::endl;
  }
  
  return;
}

void Score::saveBestScores() const
{
  std::ofstream scoreFile("saves/bestScores.save", std::ios::out);
  
  if (scoreFile.is_open())
  {
    std::set<BestScore>::const_iterator 
      iteration (bestScores.begin()), 
      fin (bestScores.end()); 
      
    for(;iteration != fin; ++iteration) 
      scoreFile << iteration->nom << " " << iteration->score << std::endl;
    
    scoreFile.close();
  }
  else 
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : le fichier des meilleurs scores n'a pu �tre ouvert ! " << std::endl;
  }
}

void Score::chargeBestScorePerso()
{
  std::ifstream scoreFile("saves/bestScorePerso.save", std::ios::in);
  
  if (scoreFile.is_open())
  {
    scoreFile >> nomJoueur >> meilleur;
    scoreFile.close();
  }
}

void Score::saveBestScorePerso() const
{
  
  std::ofstream scoreFile("saves/bestScorePerso.save", std::ios::out);
  
  if (scoreFile.is_open())
  {
    scoreFile << nomJoueur << " " << meilleur << std::endl;
    
    scoreFile.close();
  }
}