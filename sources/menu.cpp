#include "menu.h"


/*****************
 *CONSTRUCTORS
 * **************/
Menu::Menu() {}

Menu::Menu(size_t pos, const std::string& name, int touche, const std::string& desc)
 : _position (pos) , _name (name) , _touche (touche) , _description (desc) {}

/****************
 *GETTERS 
 * ************/
size_t Menu::getPosition() const { return _position; }
const std::string& Menu::getName() const { return _name; }
int Menu::getTouche() const { return _touche; }
const std::string& Menu::getDescription() const { return _description; }

/*************
 *SETTERS
 * ***********/
void Menu::setPosition(size_t pos) { _position = pos; }
void Menu::setName (const std::string& name) { _name = name; }
void Menu::setTouche (int touche) { _touche = touche; }
void Menu::setDescription (const std::string& desc) { _description = desc; }
