#ifndef MENUMANAGER_H
#define MENUMANAGER_H

#include <vector>
#include <map>
#include <string>
#include "menu.h"

class MenuManager
{
  
private:
  std::vector<Menu> _listMenu;
  std::map<int, std::string> intToKey;
	
public:
  MenuManager();
  
  void initIntToKey();
  std::string descMenu(size_t id, bool descCourte = true);
  
  void addOptionMenu(const Menu &menu);
  
  size_t getNbOptions() const;
  const Menu& getMenu(size_t) const;
  size_t getOptionMenuByNoS(int) const;
  
  bool estDansMenu(int touche) const;
  bool estDansMenu(const std::string& name) const;
  
};

#endif
