#ifndef PIECESMANAGER_H
#define PIECESMANAGER_H
#include <vector>
#include <array>
#include <string>
#include "piece.h"

// Déclaration anticipée
class Grille;

class PiecesManager 
{
private:
  std::vector<Piece> _pieces;
  size_t _probaTotale;
  std::string _configFileDefault;
  std::string _configFile;

  std::array<size_t,3> _piecesDispos;
  
public:
  static size_t nbrInstances;
  static int strtoint(char*);

  PiecesManager();
  PiecesManager(const std::string&);
  PiecesManager(const PiecesManager&);
  ~PiecesManager();
  
  PiecesManager& operator=(const PiecesManager&);
  Piece& operator[] (const size_t id);
  const Piece& operator[] (const size_t id) const;
  
  bool estPieceDispo(size_t) const;
  const std::array<size_t,3>& getPiecesDispos() const;
  bool poserSurGrille(Grille&, size_t, size_t, size_t);
  bool isGameOver (const Grille&) const;
  
  const Piece& getPiece(size_t) const;
  const std::string& getConfigFileDefault() const;
  const std::string& getConfigFile() const;
  
  void setPiecesDispos (const std::array<size_t,3>& piecesDispos);
  void selecPiecesDispos();
  void setConfigFile(const std::string&);
  
  void ajouterPiece(const Piece&);
  
  void loadConf();
  void loadConf(std::string);
  
  /**
   * \fn print() const
   * Affiche le contenu de piècesManager dans cerr dans un but de débogage
   */
  
  void print() const;

private:
  bool estVidePiecesDispos() const;
  void alloc();
  void desalloc();
  
};

#endif
