#include "listOption.h"
#include <exception>

using namespace std;

listOption::listOption() : _nb_options(0) { }

/*
 *Do : Return the Option corresponding to the pram Name or Shortcut
 *@param STRING
 *@return OPTION
 */
size_t listOption::getOptionByNoS(const string& nos) const
{
	bool found = false;
	size_t i = 0;

	while( !found && (i <= _nb_options))
	{
		found = _list[i].getName() == nos || _list[i].getShortcut() == nos;
		i++;
	}

		return found ? (i-1) : static_cast<size_t> (-1) ;
}


Option& listOption::operator[](size_t i)
{
  if (_nb_options <= i)
  {
    cerr << __FILE__ << " " << __LINE__ << " : l'option #" << i << " n'existe pas !" << endl;
    terminate();
  }
  return _list[i];
}

const Option& listOption::operator[](size_t i) const
{
  if (_nb_options <= i)
  {
    cerr << __FILE__ << " " << __LINE__ << " : l'option #" << i << " n'existe pas !" << endl;
    terminate();
  }
  return _list[i];
}

const Option& listOption::getOption(size_t id) const
{
	return _list[id];
}

/*
 * DO : Add an option to the list
 * @param   &OPTION
 * @return VOID
 */
void listOption::addOption(const Option &option)
{
	if(_nb_options == MAX_OPTION)
	{
		cerr << "Erreur : Le nombre d'option maximum est atteint ( "
			<< MAX_OPTION << ")" << endl;
		terminate();
	}

	bool exist = (getOptionByNoS(option.getName()) != static_cast<size_t> (-1))
			|| (getOptionByNoS(option.getShortcut()) !=static_cast<size_t> (-1));

	if(exist)

		cerr << "Warning : L'id " << option.getId() << " est déjà utilisé. " << endl;

	else
	{
		_list[_nb_options] = option;
		_nb_options++;
	}
}

/*
 *DO : Return the number of options
 *@param VOID
 *@return INTEGER
 */
size_t listOption::getNbOptions () const { return _nb_options; }

bool listOption::optionHasArgument(const string &opt) const 
{
	size_t i = getOptionByNoS(opt);
	return (_list[i].getType() != Option::AUCUN);
}

Option::Type listOption::optionArgumentType(const string &opt) const 
{
	size_t i = getOptionByNoS(opt);
	return _list[i].getType();
}

void listOption::print() const 
{
	cout << affiche();
}

bool listOption::parseOptions(int argc, char** argv)
{
      bool arret = false;
      int i = 1;
      while (i < argc) 
      {
		size_t x = getOptionByNoS(argv[i]);
	      
		if (x !=static_cast<size_t> (-1))
		{
			  if (_list[x].hasCallback())
			  {
				    if (_list[x].getType() != Option::AUCUN)
				    {
					    if (++i < argc)
						    _list[x].setChaine(argv[i]);
					    else 
					    {
						    cout << "Erreur dans l'utilisation des arguments ! " << endl;
						    cout << usage(argv[0]);
						    return true;
					    }
				    }
				    if (!arret && _list[x].launchCallback()) 
					    arret = true;
			  }
		}
		else 
		{
			cout << "Erreur dans l'utilisation des arguments ! " << endl;
			cout << usage(argv[0]);
			return true;
		}
	      
		i++;
      }
      
      return arret;
}

string listOption::affiche () const
{
	string retour; 
	retour += "Options : \n";
	for (size_t i = 0; i < _nb_options; i++) 
	{
		retour += _list[i].affiche();
	}
	return retour;
}

string listOption::usage (char* nom) const
{
	string temp;
	temp += "Usage : ";
	temp += nom;
	temp += " [Options]\n" + affiche();
	return temp;
}

ostream& operator<< (ostream& os, const listOption& opts)
{
	return os << opts.affiche();
}