#ifndef PARTIE_H
#define PARTIE_H

#include <string>
#include <array>
#include "listeDC.h"

// déclarations anticipées
class Grille;
class Score;
class PiecesManager;

class Partie{

	private:
		Grille* _grille;
		ListeDC<Grille> _historicGrille;
		ListeDC<Grille>::Element _posHistoricGrille;
		PiecesManager* _piecesM;
		Score* _score;
		ListeDC<std::array<size_t,2>> _listScore;
		ListeDC<std::array<size_t,2>>::Element _posListScore;
		ListeDC<std::array<size_t,3>> _historicChoicesPiece;
		ListeDC<std::array<size_t,3>>::Element _posHistoricChoicesPiece;
	
	public:

		Partie();
		Partie(PiecesManager*, Score*, Grille*);
		Partie(PiecesManager&, Score&, Grille&);

		Grille* getGrille() const;
		const ListeDC<Grille>& getHistoricGrille() const;
		ListeDC<Grille>::Element getPosHistoricGrille() const;
		PiecesManager* getPiecesM() const;
		Score* getScore() const;
		const ListeDC<std::array<size_t,2>>& getListScore() const;
		ListeDC<std::array<size_t,2>>::Element getPosListScore() const;
		const ListeDC<std::array<size_t,3>>& getHistoricChoicesPiece() const;
		ListeDC<std::array<size_t,3>>::Element getPosHistoricChoicesPiece() const;

		void setGrille(Grille*);
		void setGrille(Grille&);
		void setHistoricGrille(const ListeDC<Grille>&);
		void setPosHistoricGrille(const ListeDC<Grille>::Element&);
		void setPiecesM(PiecesManager*);
		void setPiecesM(PiecesManager&);
		void setScore(Score*);
		void setScore(Score&);
		void setListScore(const ListeDC<std::array<size_t,2>>&);
		void setPosListScore(const ListeDC<std::array<size_t,2>>::Element&);
		void setHistoricChoicesPiece(const ListeDC<std::array<size_t,3>>&);
		void setPosHistoricChoicesPiece(const ListeDC<std::array<size_t,3>>::Element&);
		
		void addHistoricGrille(const Grille*);
		void addScore(const Score*);
		void addHistoricChoicesPiece(const std::array<size_t,3>&);

		void back();
		void backHistoricGrille();
		void backListScore();
		void backHistoricChoicesPiece();
		
		void next();
		void nextHistoricGrille();
		void nextListScore();
		void nextHistoricChoicesPiece();

		void update();

		void save(std::string);
		void load(std::string);
};

#endif
