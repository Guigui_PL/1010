#include "affichage.h"
#include <iostream>
#include <curses.h>
#include <chrono>
#include <thread>
#include "score.h"
#include "grille.h"
#include "piecesManager.h"

#define COLONNES 86
#define LIGNES 38

using namespace std;

bool Affichage::nCursesActif = false;

Affichage::Affichage(bool* verrClavier) : _verrClavier(verrClavier)
{
  _hauteur = LIGNES;
  _largeur = COLONNES / 2;
  _frame = NULL;
  _frameSave = NULL;
  _cadresPieces[0] = {26, 4};
  _cadresPieces[1] = {26,17};
  _cadresPieces[2] = {26,30};
  _cadreGrille = {2, 2};
  alloc();
  startCurses();
  tracerBordures();
}

Affichage::Affichage(size_t hauteur, size_t largeur) : _hauteur(hauteur), _largeur(largeur) 
{
  _frame = NULL;
  _frameSave = NULL;
  _cadresPieces[0] = {26, 4};
  _cadresPieces[1] = {26,17};
  _cadresPieces[2] = {26,30};
  _cadreGrille = {2, 2};
  alloc();
  startCurses();
  tracerBordures();
}

void Affichage::alloc ()
{
  if (_frame == NULL)
  {
    _frame = new Pixel*[_hauteur];
    for (size_t i = 0; i < _hauteur; ++i)
    {
      _frame[i] = new Pixel[_largeur];
    }
  }
  else
  {
    cerr<< __FILE__ << " " <<  __LINE__ << " : le tableau frame a déjà été alloué !"<<endl;
  }
}
  
Affichage::~Affichage()
{
  if (nCursesActif)
    stopCurses();

  desallocFrame();
  
  if (_frameSave != NULL)
  {
    for (size_t i = 0; i < _hauteur; ++i)
    {
      delete[] _frameSave[i];
    }
    delete[] _frameSave;
    _frameSave = NULL;
  }
}

void Affichage::init_colors()
{
  init_pair(Pixel::WBLACK,   COLOR_WHITE, COLOR_BLACK);
  init_pair(Pixel::WCYAN,    COLOR_WHITE, COLOR_CYAN);
  init_pair(Pixel::WBLUE,    COLOR_WHITE, COLOR_BLUE);
  init_pair(Pixel::WYELLOW,  COLOR_WHITE, COLOR_YELLOW);  
  init_pair(Pixel::WGREEN,   COLOR_WHITE, COLOR_GREEN); 
  init_pair(Pixel::WMAGENTA, COLOR_WHITE, COLOR_MAGENTA);
  init_pair(Pixel::WRED,     COLOR_WHITE, COLOR_RED);  
  init_pair(Pixel::BWHITE,   COLOR_BLACK, COLOR_WHITE);
  init_pair(Pixel::BCYAN,    COLOR_BLACK, COLOR_CYAN);
  init_pair(Pixel::BBLUE,    COLOR_BLACK, COLOR_BLUE);
  init_pair(Pixel::BYELLOW,  COLOR_BLACK, COLOR_YELLOW);  
  init_pair(Pixel::BGREEN,   COLOR_BLACK, COLOR_GREEN); 
  init_pair(Pixel::BMAGENTA, COLOR_BLACK, COLOR_MAGENTA);
  init_pair(Pixel::BRED,     COLOR_BLACK, COLOR_RED);  
  init_pair(Pixel::RBLACK,   COLOR_RED,   COLOR_BLACK); 
  init_pair(Pixel::GBLACK,   COLOR_GREEN, COLOR_BLACK);  
}

void Affichage::startCurses ()
{
  if (!nCursesActif)
  {
    initscr();
    start_color();
    cbreak();
    noecho();
    keypad(stdscr, true);
    timeout(0);
    curs_set(0);
    mousemask(ALL_MOUSE_EVENTS | REPORT_MOUSE_POSITION, NULL);
    init_colors();
    nCursesActif = true;
    
    size_t x, y;
    getmaxyx(stdscr,y,x);
    _tailleTerm.y = y;
    _tailleTerm.x = x;
  }
}

void Affichage::stopCurses() 
{
  if (nCursesActif)
  {
    refresh();
    getch();    
    endwin();
    nCursesActif = false;
  }
}

void Affichage::desallocFrame()
{
    if (_frame != NULL)
    {
      for (size_t i = 0; i < _hauteur; ++i)
      {
	delete[] _frame[i];
      }
      delete[] _frame;
      _frame = NULL;
    }
}

const Pixel& Affichage::getPixel(size_t y, size_t x) const { return _frame[y][x]; }
size_t Affichage::getHauteur() const { return _hauteur; }
size_t Affichage::getLargeur() const { return _largeur; }

void Affichage::checkDimensions()
{
  size_t x, y;
  getmaxyx(stdscr,y,x);
  
  if ((x < _largeur*2 || y < _hauteur) && _frameSave == NULL)
  {
    _frameSave = _frame;
    _frame = NULL;
    alloc();
    afficheASCII("frames/tailleInsuffisante.ascii", Pixel::RBLACK);
    *_verrClavier = true;
  }
  else if ((x >= _largeur*2 && y >= _hauteur) && _frameSave != NULL)
  {
    desallocFrame();
    _frame = _frameSave;
    _frameSave = NULL;
    printTerminal();
    *_verrClavier = false;
  }
  else if (_tailleTerm.y != y || _tailleTerm.x != x)
  {
    _tailleTerm.y = y;
    _tailleTerm.x = x;
    viderTerminal();
    printTerminal();
  }
}

void Affichage::afficheASCII(const string& nomFichier, size_t debutY, size_t debutX, size_t finY, size_t finX, Pixel::Color couleur)
{
  ifstream flux(nomFichier.c_str(), ios::in);
  afficheASCII(flux, debutY, debutX, finY, finX, couleur);
  flux.close();
}

void Affichage::afficheASCII(ifstream& flux, size_t debutY, size_t debutX, size_t finY, size_t finX, Pixel::Color couleur)
{
  if (_frame != NULL)
  {
    if (flux)
    {
      string ligne;
      size_t length, i = 0;
      char cara1, cara2;
      
      if (finY > _hauteur)
      {
	finY = _hauteur-1;
	cerr << __FILE__ << " " << __LINE__ << " : Le texte ascii dépasse de la fenêtre du programme ! " << endl;
      }
      if (finX > _largeur)
      {
	finX = _largeur;
	cerr << __FILE__ << " " << __LINE__ << " : Le texte ascii dépasse de la fenêtre du programme ! " << endl;
      }
      
      while (getline(flux, ligne) && i <= finY-debutY)
      {
	length = ligne.length();
	for(size_t j = 0; j <= finX-debutX; ++j)
	{
	    if (j*2 < length) cara1 = ligne.at(j*2);
	    else cara1 = ' ';
	    if (j*2+1 < length) cara2 = ligne.at(j*2+1);
	    else cara2 = ' ';
	    
	    Pixel pixel(couleur, cara1, cara2);
	    _frame[i+debutY][j+debutX] = pixel;
	}
	++i;
      }
      
      while (i <= finY-debutY)
      {
	for(size_t j = debutX; j <= finX; ++j)
	    _frame[i+debutY][j] = Pixel(couleur, ' ', ' ');
	++i;
      }
      
      printTerminal();
    }
    else
    {
      cerr << __FILE__<<" "<<__LINE__ <<" : Impossible de lire le fichier !" << endl;
    }
  }
  else
  {
    cerr << __FILE__<<" "<<__LINE__ <<" : La frame n'existe pas !" << endl;
  }
}

void Affichage::afficheASCII(const string& nomFichier, Pixel::Color couleur)
{
  ifstream flux(nomFichier.c_str(), ios::in);
  afficheASCII(flux, 1, 1, _hauteur-2, _largeur-2, couleur);
  flux.close();
}

void Affichage::afficheASCII(ifstream& flux, Pixel::Color couleur)
{
  afficheASCII(flux, 1, 1, _hauteur-2, _largeur-2, couleur);
}


void Affichage::tracerCadre (size_t debutY, size_t debutX, size_t largeurCadre, Pixel::Color couleur)
{
  Pixel pixel = Pixel(couleur, ' ', ' ');
  
  if (debutY+largeurCadre > _hauteur)
  {
    largeurCadre = _hauteur-debutY;
    cerr << __FILE__ << " " << __LINE__ << " : Le cadre dépasse de la fenêtre du programme ! " << endl;
  }
  if (debutX+largeurCadre > _largeur)
  {
    largeurCadre = _largeur-debutX;
    cerr << __FILE__ << " " << __LINE__ << " : Le cadre dépasse de la fenêtre du programme ! " << endl;
  }
  
  --largeurCadre;
  for (size_t i = 0; i <= largeurCadre; ++i)
  {
    _frame[debutY][debutX+i] = pixel;
    _frame[debutY+largeurCadre][debutX+i] = pixel;
    _frame[debutY+i][debutX] = pixel;
    _frame[debutY+i][debutX+largeurCadre] = pixel;
  }
  
  printTerminal();
}

void Affichage::remplirZone (size_t debutY, size_t debutX, size_t hauteurZone, size_t largeurZone, Pixel::Color couleur)
{
  if (debutY+hauteurZone > _hauteur)
  {
    hauteurZone = _hauteur-debutY;
    cerr << __FILE__ << " " << __LINE__ << " : La zone dépasse de la fenêtre du programme ! " << endl;
  }
  if (debutX+largeurZone > _largeur)
  {
    largeurZone = _largeur-debutX;
    cerr << __FILE__ << " " << __LINE__ << " : La zone dépasse de la fenêtre du programme ! " << endl;
  }
  
  for (size_t i = 0; i < hauteurZone; ++i)
  {
    for (size_t j = 0; j < largeurZone; ++j)
    {
      _frame[debutY+i][debutX+j] = Pixel(couleur, ' ', ' ');
    }
  }
  
  printTerminal();
}

void Affichage::tracerBordures ()
{
  Pixel pixel1(Pixel::WBLUE, '/', '/');
  Pixel pixel2(Pixel::WBLUE, '=', '=');
  Pixel pixel3(Pixel::WBLUE, '|', '|');
  Pixel pixel4(Pixel::WBLUE, '\\', '\\');
  
  _frame[0][0] = pixel1;
  _frame[0][_largeur-1] = pixel4;
  _frame[_hauteur-1][0] = pixel4;
  _frame[_hauteur-1][_largeur-1] = pixel1;
  
  for (size_t i = 1; i < _largeur-1; ++i)
  {
    _frame[0][i] = pixel2;
    _frame[_hauteur-1][i] = pixel2;
  }
  for (size_t i = 1; i < _hauteur-1; ++i)
  {
    _frame[i][0] = pixel3;
    _frame[i][_largeur-1] = pixel3;
  }
  
  printTerminal();
}

void Affichage::viderCentre ()
{
  remplirZone(1, 1, _hauteur-2, _largeur-2, Pixel::WBLACK);
}

void Affichage::tracerCadreGrille ()
{
  tracerCadre(_cadreGrille.y, _cadreGrille.x, 22, Pixel::BMAGENTA);
}

void Affichage::afficherGrille (const Grille& grille)
{
  for (size_t i = 0; i < 10; ++i)
  {
    for (size_t j = 0; j < 10; ++j)
    {
      
      Case c = grille.getCase(i,j);
      
      for (size_t k = i*2 + _cadreGrille.y + 1; k <= i*2 + _cadreGrille.y + 2; ++k)
      {
	for (size_t l = j*2 + _cadreGrille.x + 1; l <= j*2 +_cadreGrille.x + 2; ++l)
	{
	  _frame[k][l] = Pixel(c.getCouleurPixel());
	}
      }
      
    }
  }
  
  printTerminal();
}

void Affichage::ajouteTblPixels (const vector<Pixel>& pixel, const size_t ligne, const size_t col)
{
  for (size_t i = col; i < col+pixel.size() && i < _largeur-2; ++i)
    _frame[ligne][i] = pixel[i-col];
  
  printTerminal();
}

void Affichage::ajouterTexteCentreInFrame (const string& texte, size_t ligne, size_t col, size_t colFin)
{
  string copie = texte;
  size_t colDeb = (colFin * 2 - col * 2 - copie.length()) / 2 + col * 2;
  if (colDeb % 2 == 1) copie.insert(0, " ");
  ajouterTexteInFrame(copie, ligne, colDeb/2);
}

void Affichage::ajouterTexteInFrame (const string& texte, size_t ligne, size_t col)
{
  ajouteTblPixels(Pixel::texteInPixels(texte, Pixel::GBLACK), ligne, col);
}

void Affichage::afficherScore (const Score& score)
{
  remplirZone(10, 25, 3, 15, Pixel::WBLACK);
  
  string texte = "Score : ";
  texte += to_string(static_cast<int> (score.getTotal()));
  ajouterTexteCentreInFrame(texte, 11, 25, _largeur-2);
  
  texte = "Meilleur : ";
  texte += to_string(static_cast<int>(score.getMeilleur()));
  ajouterTexteCentreInFrame(texte, 12, 25, _largeur-2);
  
  texte = "Joueur : ";
  texte += score.getNomJoueur();
  ajouterTexteCentreInFrame(texte, 10, 25, _largeur-2);
  
  printTerminal();
}

void Affichage::tracerCadresPieces ()
{
  tracerCadre(_cadresPieces[0].y, _cadresPieces[0].x, 9, Pixel::BCYAN);
  tracerCadre(_cadresPieces[1].y, _cadresPieces[1].x, 9, Pixel::BCYAN);
  tracerCadre(_cadresPieces[2].y, _cadresPieces[2].x, 9, Pixel::BCYAN);
}

void Affichage::remplirCadresPieces ()
{
  for (size_t i = 0; i < 3; ++i)
    viderCadrePiece(i);
}

void Affichage::viderCadrePiece (size_t numero)
{
  if (numero >= 3)
  {
   cerr << "Ce numéro de pièce posable ne peut exister ; " << numero << " !" << endl;
   return;
  }
  
  remplirZone (_cadresPieces[numero].y+1, _cadresPieces[numero].x+1, 7, 7, Pixel::BWHITE);
}

void Affichage::afficherTexteCadres ()
{
  ajouterTexteCentreInFrame("<=( Touche 1 )=>", _cadresPieces[0].y+9, _cadresPieces[0].x, _cadresPieces[0].x+9);
  ajouterTexteCentreInFrame("<=( Touche 2 )=>", _cadresPieces[1].y+9, _cadresPieces[1].x, _cadresPieces[1].x+9);
  ajouterTexteCentreInFrame("<=( Touche 3 )=>", _cadresPieces[2].y+9, _cadresPieces[2].x, _cadresPieces[2].x+9);
}

void Affichage::afficherPieceDispo (const PiecesManager& pieMan, size_t numero)
{
  if (numero >= 3)
  {
   cerr << "Ce numéro de pièce posable ne peut exister : " << numero << " !" << endl;
   return;
  }
  
  array<size_t,3> piecesDispos = pieMan.getPiecesDispos();
  Coor debut;
  size_t idPiece = piecesDispos[numero];
  
  if (idPiece != static_cast<size_t> (-1))
  {
    Piece piece = pieMan[idPiece];
    debut.y =  (5 - piece.getHauteur()) / 2 + _cadresPieces[numero].y + 2;
    debut.x =  (5 - piece.getLargeur()) / 2 + _cadresPieces[numero].x + 2;
    for (size_t j = 0; j < piece.getHauteur(); ++j)
    {
      for (size_t k = 0; k < piece.getLargeur(); ++k)
      {
	if (!piece[j][k].estVide())
	  _frame[j+debut.y][k+debut.x] = Pixel (piece[j][k].getCouleurPixel(), '[', ']');
      }
    }
  }
  
  printTerminal();
}

void Affichage::afficherPiecesDispos (const PiecesManager& pieMan)
{
  for (size_t i = 0; i < 3; ++i)
    afficherPieceDispo (pieMan, i);
}

bool Affichage::afficherPieceGrille (const PiecesManager& pieMan, size_t numero, Affichage::Coor coordonnees)
{
  size_t id;
  if (numero < 3 && (id = pieMan.getPiecesDispos()[numero]) != static_cast<size_t> (-1))
  {
    if (coordonnees.y + pieMan[id].getHauteur() > 10 || coordonnees.x + pieMan[id].getLargeur() > 10 || coordonnees.y < 0 || coordonnees.x < 0)
      return false;
    
    for (size_t i = 0; i < pieMan[id].getHauteur(); ++i)
    {
      for (size_t j = 0; j < pieMan[id].getLargeur(); ++j)
      {
	
	Case c = pieMan[id].getCase(i,j);
	
	if (!c.estVide())
	{
	  _frame[i*2 + coordonnees.y * 2 + _cadreGrille.y + 1][j*2 + coordonnees.x * 2 + _cadreGrille.x + 1] = Pixel(c.getCouleurPixel(), '/', '-');
	  _frame[i*2 + coordonnees.y * 2 + _cadreGrille.y + 1][j*2 + coordonnees.x * 2 + _cadreGrille.x + 2] = Pixel(c.getCouleurPixel(), '-', '\\');
	  _frame[i*2 + coordonnees.y * 2 + _cadreGrille.y + 2][j*2 + coordonnees.x * 2 + _cadreGrille.x + 1] = Pixel(c.getCouleurPixel(), '\\', '-');
	  _frame[i*2 + coordonnees.y * 2 + _cadreGrille.y + 2][j*2 + coordonnees.x * 2 + _cadreGrille.x + 2] = Pixel(c.getCouleurPixel(), '-', '/');
	}
	
      }
    }
    
    printTerminal();
    return true;
  }
  else
  {
    cerr << __FILE__ << " " << __LINE__ << " : vous ne pouvez afficher sur la grille qu'une pièce disponible !" << endl;
  }
  return false;
}

void Affichage::coloration (Pixel::Color couleur)
{
  for (size_t i = 0; i < _hauteur; ++i)
  {
    for (size_t j = 0; j < _largeur; ++j)
    {
      _frame[i][j].setCouleur(couleur);
    }
  }
  printTerminal();
}

Affichage::Coor Affichage::getPositionFrame () const
{
  Coor decalage{0,0};
  if ( ((static_cast<int> (_tailleTerm.y) - static_cast<int> (_hauteur) ) / 2) > 0 ) decalage.y = ( _tailleTerm.y - _hauteur ) / 2;
  if ( ((static_cast<int> (_tailleTerm.x) - static_cast<int> (_largeur) * 2 ) / 2) > 0 ) decalage.x = ( _tailleTerm.x - _largeur * 2 ) / 2;
  
  return decalage;
}

void Affichage::viderTerminal () const
{
  size_t x, y;
  getmaxyx(stdscr,y,x);
  
  for (size_t i = 0; i < y; ++i)
  {
    for (size_t j = 0; j < x; ++j)
    {
      move (i, j);
      attron(COLOR_PAIR(Pixel::WBLACK));
      addstr(" ");
    }
  }
}

void Affichage::printTerminal () const
{
  Coor decalage = getPositionFrame();
  
  for (size_t i = 0; i < _hauteur; ++i)
  {
    for (size_t j = 0; j < _largeur; ++j)
    {
      move (i + decalage.y, j*2 + decalage.x);
      attron(COLOR_PAIR(_frame[i][j].getCouleur()));
      addstr(_frame[i][j].getCaracteres().c_str());
    }
  }
}