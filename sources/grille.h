#ifndef GRILLE_H
#define GRILLE_H
#include "matriceCases.h"
#include "piece.h"

// déclaration anticipée 
class Score;

class Grille : public MatriceCases
{
  
  private:
    Score* _score;
    
    void updateGrille ();
    void supprLigne(size_t ligne);
    void supprColonne(size_t colonne);
  
  public:
    Grille(Score*);
    
    Grille& operator= (const Grille& grille);
    
    bool placerPiece (size_t y, size_t x, const Piece& piece);
    bool estPosable (size_t y, size_t x, const Piece& piece) const;
    bool estPosable (const Piece& piece) const;
    
    void setScore(Score* score);
    
    Score* getScore() const;
    
};

#endif