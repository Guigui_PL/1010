#ifndef SCORE_H
#define SCORE_H
#include <string>
#include <set>

class Score
{
public:
  typedef struct bestScore
  {
    std::string nom;
    size_t score;
    
    bool operator< (const bestScore&) const;
    bool operator> (const bestScore&) const;
  } BestScore;
  
private:
  size_t total;
  size_t meilleur;
  std::string nomJoueur;
  std::set<BestScore, std::greater<BestScore>> bestScores;

public:
  static size_t tblScores[7];

  Score();
  Score(const std::string&);
  Score(size_t, size_t, const std::string&);
  ~Score();
  
  size_t getTotal() const;
  size_t getMeilleur() const;
  const std::string& getNomJoueur() const;
  const std::set<BestScore, std::greater<BestScore>>& getBestScores() const;

  void setTotal(size_t);
  void setMeilleur(size_t);
  void setNomJoueur(std::string);

  void updateScore(size_t, size_t);

  void chargeBestScores();
  void saveBestScores() const;
  void ajouterBest(std::string nom, size_t score);
  
  void chargeBestScorePerso();
  void saveBestScorePerso() const;
};

#endif
