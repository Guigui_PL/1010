#include "grille.h"
#include <iostream>
#include "score.h"

Grille::Grille(Score* score) : MatriceCases(10,10), _score(score) {}

Grille& Grille::operator= (const Grille& grille)
{
  if (this != &grille)
  {
    this->MatriceCases::operator=(grille);
    _score = grille._score;
  }
  return *this;
}
    
bool Grille::placerPiece (size_t y, size_t x, const Piece& piece)
{
  bool peutEtrePosee = estPosable(y, x, piece);
  
  if (peutEtrePosee)
  {
    for (size_t i = 0; i < piece.getHauteur(); i++)
    {
      for (size_t j = 0; j < piece.getLargeur(); j++)
      {
	if(!piece.getCase(i, j).estVide()) _cases[i+y][j+x] = piece.getCase(i, j);
      }
    }
    
    updateGrille();
    
    return true;
  }
  
  return false;
}

bool Grille::estPosable (size_t y, size_t x, const Piece& piece) const
{
  if (x + piece.getLargeur() > 10 || y + piece.getHauteur() > 10 || y > 10 || x > 10) return false;
  
  bool peutEtrePosee = true;
  for (size_t i = y; i < y + piece.getHauteur(); i++)
  {
    for (size_t j = x; j < x + piece.getLargeur(); j++)
    {
      if (!_cases[i][j].estVide() && !piece.getCase(i-y, j-x).estVide()) peutEtrePosee = false;
    }
  }
  return peutEtrePosee;
}

bool Grille::estPosable (const Piece& piece) const
{
  bool peutEtrePosee = false;
  size_t i = 0;
  
  while (i <= 10 - piece.getHauteur() && !peutEtrePosee)
  {
    size_t j = 0;
    while (j <= 10 - piece.getLargeur() && !peutEtrePosee)
    {
      if (estPosable(i, j, piece))
	peutEtrePosee = true;
      j++;
    }
    i++;
  }
  return peutEtrePosee;
}

void Grille::updateGrille ()
{
  bool colSuppr[10];
  bool ligSuppr[10];
  size_t nbrLigSuppr = 0;
  size_t nbrColSuppr = 0;
  
  for (size_t i = 0; i < 10; ++i)
  {
    colSuppr[i] = true;
    ligSuppr[i] = true;
  }
  
  for (size_t i = 0; i < 10; i++)
  {
    for (size_t j = 0; j < 10; j++)
    {
      if (_cases[j][i].estVide()) colSuppr[i] = false;
      if (_cases[i][j].estVide()) ligSuppr[i] = false;
    }
    if (colSuppr[i]) nbrColSuppr++;
    if (ligSuppr[i]) nbrLigSuppr++;
  }
  
  if (nbrColSuppr || nbrLigSuppr)
  {
    for (size_t i = 0; i < 10; i++)
    {
      if (colSuppr[i]) supprColonne(i);
      if (ligSuppr[i]) supprLigne(i);
    }
    
    _score->updateScore(nbrLigSuppr, nbrColSuppr);
  }
}

void Grille::supprLigne(size_t ligne)
{
  Case caseVide;
  for (size_t i = 0; i < 10; i++)
  {
    _cases[ligne][i] = caseVide;
  }
}

void Grille::supprColonne(size_t colonne)
{
  Case caseVide;
  for (size_t i = 0; i < 10; i++)
  {
    _cases[i][colonne] = caseVide;
  }
}

void Grille::setScore(Score* score)
{
  _score = score;
}
    
Score* Grille::getScore() const
{
  return _score;
}