#ifndef AFFICHAGE_H
#define AFFICHAGE_H
#include <string>
#include <fstream>
#include <vector>
#include "pixel.h"

// déclarations anticipées
class Score;
class Grille;
class PiecesManager;

/**
 * \class Affichage
 * \brief Gère l'affichage du jeu à l'aide de nCurses
 */

class Affichage 
{
public:
  typedef struct coordonnees
  {
     size_t y;
     size_t x;
  } Coor;
  
private:
  size_t _hauteur;
  size_t _largeur;
  Pixel** _frame;
  Pixel** _frameSave;
  bool* _verrClavier;
  Coor _cadresPieces[3];
  Coor _cadreGrille;
  Coor _tailleTerm;
  
public:
  static bool nCursesActif;
  
  /*
   * Constructeurs & destructeurs
   */
  
  Affichage(bool* verrClavier);
  Affichage(size_t hauteur, size_t largeur);
  ~Affichage();
  
  /**
   * \fn alloc()
   * Permet d'allouer la matrice _frame
   */
  
  void alloc();
  
  /**
   * \fn desallocFrame()
   * Permet de désallouer la matrice _frame et éventuellement la matrice _frameSave (si allouée)
   */
  
  void desallocFrame();
  
  /*
   * Accesseurs des différents attributs
   */
  
  const Pixel& getPixel(size_t y, size_t x) const;
  size_t getHauteur() const;
  size_t getLargeur() const;
  
  /*
   * Fonctions gérant l'initialisement et la fermeture de nCurses
   */
  
  static void init_colors();
  void startCurses();
  static void stopCurses();
  
  /**
   * \fn checkDimensions()
   * Vérifie que la taille du terminal soit suffisante et affiche un message d'erreur dans le cas contraire
   */
  
  void checkDimensions();
  
  /*
   * Fonctions d'affichage de fichiers ascii : écrivent le contenu d'un fichier texte dans l'attribut _frame
   */
  
  void afficheASCII(const std::string& nomFichier, size_t debutY, size_t debutX, size_t finY, size_t finX, Pixel::Color couleur);
  void afficheASCII(std::ifstream& flux, size_t debutY, size_t debutX, size_t finY, size_t finX, Pixel::Color couleur);
  
  void afficheASCII(const std::string& nomFichier, Pixel::Color couleur);
  void afficheASCII(std::ifstream& flux, Pixel::Color couleur);
  
  /*
   * Fonctions permettant de tracer un cadre puis de le remplir 
   */
  
  void tracerCadre (size_t debutY, size_t debutX, size_t largeurCadre, Pixel::Color couleur);
  void remplirZone (size_t debutY, size_t debutX, size_t hauteurZone, size_t largeurZone, Pixel::Color couleur);
  
  /*
   * Fonctions permettant de tracer le contour du jeu et d'effacer ce qu'il y a à l'intérieur des bordures 
   */
  
  void tracerBordures();
  void viderCentre();
  
  /*
   * Fonctions gérant l'affichage de la grille
   */
  
  void tracerCadreGrille();
  void afficherGrille(const Grille& grille);
  
  /*
   * Fonctions permettant d'ajouter du texte dans la _frame et d'afficher le score
   */
  
  void ajouteTblPixels (const std::vector<Pixel>& pixel, size_t ligne, size_t col);
  void ajouterTexteCentreInFrame (const std::string& texte, size_t ligne, size_t col, size_t colFin);
  void ajouterTexteInFrame (const std::string& texte, size_t ligne, size_t col);
  void afficherScore (const Score& score);
  
  /*
   * Fonctions gérant les 3 cadres des pièces en bas du jeu
   */
  
  void tracerCadresPieces ();
  void remplirCadresPieces ();
  void viderCadrePiece (size_t numero);
  void afficherTexteCadres ();
  void afficherPieceDispo (const PiecesManager& pieMan, size_t numero);
  void afficherPiecesDispos (const PiecesManager& pieMan);
  
  /*
   * Placement d'une pièce
   */
  
  bool afficherPieceGrille (const PiecesManager& pieMan, size_t numero, Coor coordonnees);
  
  /*
   * Coloration de la frame 
   */
  
  void coloration (Pixel::Color couleur);
  
  /*
   * Position de la frame dans le terminal
   */
  
  Coor getPositionFrame () const;
  
  /*
   * Effacer le contenu du terminal
   */
  
  void viderTerminal () const;
  
  /**
   * \fn printTerminal() const
   * Affiche la _frame dans le terminal à l'aide de nCurses
   */
  
  void printTerminal() const;
};

#endif 
