#include "piecesManager.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <exception>
#include <ctime>
#include "grille.h"

#define CONFIG_FILE_DEFAULT "cfg/default.cfg"

size_t PiecesManager::nbrInstances = 0;

PiecesManager::PiecesManager() : _probaTotale(0), _configFileDefault(CONFIG_FILE_DEFAULT)
{
  if (nbrInstances == 0)
  {
    for (size_t i = 0; i < 3; i++)
    {
      _piecesDispos[i] =static_cast<size_t> (-1);
    }
    ++nbrInstances;
  }
  else
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : il est interdit de créer plusieurs instances du gestionnaire de pièces !" << std::endl;
  }
}

PiecesManager::PiecesManager(const std::string& configFile) : _probaTotale(0), _configFileDefault(CONFIG_FILE_DEFAULT)
{
  if (nbrInstances == 0)
  {
    for (size_t i = 0; i < 3; i++)
    {
      _piecesDispos[i] =static_cast<size_t> (-1);
    }
    
    loadConf(configFile);
    
    ++nbrInstances;
  }
  else
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : il est interdit de créer plusieurs instances du gestionnaire de pièces !" << std::endl;
  }
}


PiecesManager::PiecesManager(const PiecesManager& piecesManager)
{
  std::cerr << __FILE__ << " " << __LINE__ << " : la copie du gestionnaire de pièces est interdite !" << std::endl;
}

PiecesManager::~PiecesManager()
{
  --nbrInstances;
}
  
PiecesManager& PiecesManager::operator=(const PiecesManager& piecesManager)
{
  std::cerr << __FILE__ << " " << __LINE__ << " : la copie du gestionnaire de pièces est interdite !" << std::endl;
  std::terminate();
  return *this;
}

Piece& PiecesManager::operator[] (const size_t id)
{
  if (id >= _pieces.size())
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : la pièce " << id << " n'existe pas !"<<std::endl;
    std::terminate();
  }
  return _pieces[id];
}

const Piece& PiecesManager::operator[] (const size_t id) const
{
  if (id >= _pieces.size())
  {
    std::cerr << __FILE__ << " " << __LINE__ << " : la pièce " << id << " n'existe pas !"<<std::endl;
    std::terminate();
  }
  return _pieces[id];
}
  
bool PiecesManager::estPieceDispo(size_t numero) const
{
  for (size_t i = 0; i < 3; i++)
  {
    if (_piecesDispos[i] == numero) return true;
  }
  return false;
}

const std::array<size_t,3>& PiecesManager::getPiecesDispos() const
{
  return _piecesDispos;
}

bool PiecesManager::estVidePiecesDispos () const
{
  bool vide = true;
  size_t i = 0;
  
  while (i < 3 && vide)
  {
    if (_piecesDispos[i] !=static_cast<size_t> (-1)) 
      vide = false;
    i++;
  }
  
  return vide;
}

bool PiecesManager::poserSurGrille(Grille& grille, size_t x, size_t y, size_t pieceId)
{
  if (pieceId < 3 && _piecesDispos[pieceId] !=static_cast<size_t> (-1))
  {
    bool reussi = false;
    reussi = grille.placerPiece(y, x, _pieces[_piecesDispos[pieceId]]);
    
    if (reussi)
    {
      _piecesDispos[pieceId] =static_cast<size_t> (-1);
      
      if (estVidePiecesDispos())
	selecPiecesDispos();
      
      return true;
    }
  }
  
  return false;
}

bool PiecesManager::isGameOver (const Grille& grille) const
{
  bool fini = true;
  size_t i = 0;
  
  while (i < 3 && fini)
  {
    if (_piecesDispos[i] !=static_cast<size_t> (-1))
    {
	if (grille.estPosable(_pieces[_piecesDispos[i]])) fini = false;
    }
    ++i;
  }
  return fini;
}
  
const Piece& PiecesManager::getPiece(size_t pieceId) const
{
  return _pieces[pieceId];
}
  
const std::string& PiecesManager::getConfigFileDefault() const
{
  return _configFileDefault;
}

const std::string& PiecesManager::getConfigFile() const 
{ 
  return _configFile; 
}
  
void PiecesManager::selecPiecesDispos()
{
  if (_pieces.size() == 0) // Si le tableau de pièces est vide, on charge les pièces par défaut
    loadConf();
  
  if (_pieces.size() > 0 && _probaTotale != 0)
  {
    size_t proba, auSort, j;
    srand(time(NULL));
    bool trouvee;
    
    for (size_t i = 0; i < 3; i++)
    {
      auSort = rand()%(_probaTotale) + 1;
      j = proba = 0;
      trouvee = false;
      
      do
      {
	proba += _pieces[j].getProbabilite();
	if (auSort <= proba)
	{
	  _piecesDispos[i] = j;
	  trouvee = true;
	}
      } while (!trouvee && ++j < _pieces.size());
    }
  }
  else if (_pieces.size() == 0)
  {
    std::cerr << __FILE__ << " " << __LINE__ << "Impossible de sélectionner les pièces Dispos : il n'y a aucune pièce dans le jeu !"<<std::endl;
  }
}
  
void PiecesManager::setPiecesDispos (const std::array<size_t,3>& piecesDispos)
{
  _piecesDispos = piecesDispos;
}

void PiecesManager::setConfigFile(const std::string& configFile) 
{ 
  _configFile = configFile;
}

void PiecesManager::ajouterPiece(const Piece &piece)
{
  _pieces.push_back(piece);
  _probaTotale += piece.getProbabilite();
}

int PiecesManager::strtoint(char * c)
{
	int i;
	std::istringstream convert(c);
	if( !(convert >> i))
		i = -1;

	return i;
}

void PiecesManager::loadConf()
{
  loadConf(_configFileDefault);
}

void PiecesManager::loadConf(std::string configFile)
{
	std::vector<Piece>().swap(_pieces);
	_probaTotale = 0;
	std::ifstream file_conf(configFile.c_str());
	if( file_conf.is_open())
	{
		_configFile = configFile;
		std::string s_container;
		int i_container;
		getline(file_conf, s_container);
		std::istringstream convert(s_container);
		if( !(convert >> i_container))
			i_container = 0;

		for(int i = 0; i < i_container; i++)
		{
			char c[2];
			file_conf >> c;
			int height = strtoint(c);

			file_conf >> c;
			int width = strtoint(c);

			file_conf >> c;
			int rotation = strtoint(c);
			rotation = rotation / 90;

			file_conf >> c;
			int color = strtoint(c);

			file_conf >> c;
			int proba = strtoint(c);

			int ** readCases = new int * [height];
			for(int g = 0; g < height; g++)
				readCases[g] = new int [width];

			for(int h = 0;  h < height; h++)
			{
				for(int w = 0; w < width; w++)
				{
					char p[1];
		
					file_conf >> p;
					int vide = strtoint(p);

					readCases[h][w] = vide;
				}
			}

			for(int r = 0; r <= rotation; r++)
			{
				if( r % 2 == 0)
				{
					Piece piece(height, width, Piece::Probabilite(proba));

					for(int h = 0; h < height; h++)
					{
						for(int w = 0; w < width; w++)
						{
							Case c_case(readCases[h][w] == 1 ? false : true, Case::Couleur(color));
							piece[h][w] = c_case;
						}
					}
					_pieces.push_back(piece);
					
					_probaTotale += piece.getProbabilite();
				}
				else
				{
					Piece piece(width, height, Piece::Probabilite(proba));

					for(int h = 0; h < height; h++)
					{
						for(int w = 0; w < width; w++)
						{
							Case c_case(readCases[h][w] == 1 ? false : true, Case::Couleur(color));
							piece[w][h] = c_case;
						}
					}
					_pieces.push_back(piece);

					_probaTotale += piece.getProbabilite();
				}
			}
			
			for(int g = 0; g < height; g++)
			  delete[] readCases[g];
			delete[] readCases;

		}
		
		selecPiecesDispos();
	}
	else
	{
		std::cout << "Impossible d'ouvrir le fichier de configuration." << std::endl;
		std::terminate();
	}

}

void PiecesManager::print () const
{
  std::cerr << " --- contenu de ce PiecesManager --- " << std::endl
    << "Nombre de pièces : " << _pieces.size() << std::endl
    << "Probabilité totale : " << _probaTotale << std::endl
    << "Pièces : " << std::endl;
  
  for (size_t i = 0; i < _pieces.size(); ++i)
  {
    std::cerr << "- Pièce n°" << i << std::endl;
    _pieces[i].print();
  }
  
  std::cerr << std::endl << "Liste des pièces disponibles : " << std::endl;
  
  for (int i = 0; i < 3; ++i)
  {
    if (_piecesDispos[i] !=static_cast<size_t> (-1))
      std::cerr << _piecesDispos[i] << " ";
  }
  std::cerr << std::endl;
}
