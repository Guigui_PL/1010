#include "piece.h"
#include <iostream>

Piece::Piece () : MatriceCases(), _probabilite(Piece::NULLE) {}

Piece::Piece(size_t hauteur, size_t largeur, Piece::Probabilite probabilite) : MatriceCases(hauteur, largeur),  _probabilite(probabilite) {}

Piece::Piece(const Piece& piece) : MatriceCases(piece), _probabilite(piece._probabilite) {}
  
Piece& Piece::operator=(const Piece& piece)
{
  if (this != &piece)
  {
    this->MatriceCases::operator=(piece);
    _probabilite = piece._probabilite;
  }
  return *this;
}


/**
 * Mutateurs des différents attributs
 */

void Piece::setProbabilite(Piece::Probabilite probabilite) 
{
  _probabilite = probabilite;
}
  
/**
 * Accesseurs des différents attributs
 */

Piece::Probabilite Piece::getProbabilite() const { return _probabilite; }

// Debug

void Piece::print () const
{
  std::cerr << "\tProbabilité : " << _probabilite << std::endl
    << "\tHauteur : " << _hauteur << std::endl
    << "\tLargeur : " << _largeur << std::endl
    << "\tCases : " << std::endl << std::endl;
  for (size_t i = 0; i < _hauteur; ++i)
  {
    std::cerr << "\t  ";
    for (size_t j = 0; j < _largeur; ++j)
    {
      std::cerr << static_cast<size_t> (_cases[i][j].getCouleur());
    }
    std::cerr << std::endl;
  }
  std::cerr << std::endl;
}