#include "matriceCases.h"
#include <iostream>
#include <exception>

MatriceCases::MatriceCases ()
{
  _largeur = 0; 
  _hauteur = 0; 
  _cases = NULL;
}

MatriceCases::MatriceCases(size_t hauteur, size_t largeur)
{
  _largeur = largeur; 
  _hauteur = hauteur;
  _cases = NULL;
  alloc();
}

MatriceCases::MatriceCases(const MatriceCases& matriceCases)
{
  _largeur = matriceCases._largeur; 
  _hauteur = matriceCases._hauteur;
  _cases = NULL;
  alloc();
  for (size_t i = 0; i < _hauteur; i++)
  {
    for (size_t j = 0; j < _largeur; j++)
    {
      _cases[i][j] = matriceCases._cases[i][j];
    }
  }
}
  
MatriceCases& MatriceCases::operator=(const MatriceCases& matriceCases)
{
  if (this != &matriceCases)
  {
    desalloc();
    _largeur = matriceCases._largeur;
    _hauteur = matriceCases._hauteur;
    alloc();
    for (size_t i = 0; i < _hauteur; i++)
    {
      for (size_t j = 0; j < _largeur; j++)
      {
	_cases[i][j] = matriceCases._cases[i][j];
      }
    }
  }
  return *this;
}

Case* MatriceCases::operator[](size_t y)
{
  if (y >= _hauteur || _cases == NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Impossible d'obtenir la case*["<<y<<"]"<<std::endl;
    return NULL;
  }
  return _cases[y];
}

const Case* MatriceCases::operator[](size_t y) const
{
  if (y >= _hauteur || _cases == NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Impossible d'obtenir la case*["<<y<<"]"<<std::endl;
    return NULL;
  }
  return _cases[y];
}

MatriceCases::~MatriceCases()
{
  desalloc();
}
  
void MatriceCases::alloc()
{
  if (_cases == NULL)
  {
    _cases = new Case*[_hauteur];
    for (size_t i = 0; i < _hauteur; i++)
    {
      _cases[i] = new Case[_largeur];
    }
  }
  else 
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Le tableau de cases a déjà été alloué"<<std::endl;
  }
}

void MatriceCases::desalloc ()
{
  for (size_t i = 0; i < _hauteur; i++)
  {
    delete[] _cases[i];
  }
  delete[] _cases;
  _cases = NULL;
}

/**
 * Mutateurs des différents attributs
 */

void MatriceCases::setHauteur(size_t hauteur)
{
  if (_cases != NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : La hauteur ne peut être plus être modifiée une fois le tableau de cases alloué "<<std::endl;
  }
  else _hauteur = hauteur;
}

void MatriceCases::setLargeur(size_t largeur)
{
  if (_cases != NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : La largeur ne peut être plus être modifiée une fois le tableau de cases alloué"<<std::endl;
  }
  else _largeur = largeur;
}

void MatriceCases::setCase(size_t y, size_t x, const Case& c)
{
  if (x >= _largeur || y >= _hauteur || _cases == NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Impossible de modifier la case["<<y<<"]["<<x<<"]"<<std::endl;
  }
  else _cases[y][x] = c;
}
  
/**
 * Accesseurs des différents attributs
 */

const Case& MatriceCases::getCase(size_t y, size_t x) const
{
  if (x >= _largeur || y >= _hauteur || _cases == NULL)
  {
    std::cerr<< __FILE__<<" "<<__LINE__ <<" : Impossible d'obtenir la case["<<y<<"]["<<x<<"]"<<std::endl;
    std::terminate();
  }
  return _cases[y][x];
}

size_t MatriceCases::getLargeur() const { return _largeur; }
size_t MatriceCases::getHauteur() const { return _hauteur; }
