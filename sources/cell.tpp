#include <iostream>

template <typename Type>
Cell<Type>::Cell() : _succ(NULL), _pred(NULL) {}

template <typename Type>
Cell<Type>::Cell(const Type& info) : _info(info), _succ(NULL), _pred(NULL) {}

template <typename Type>
Cell<Type>::Cell(const Type& info, Cell<Type>* succ, Cell<Type>* pred) : _info(info), _succ(succ), _pred(pred) {}

template <typename Type>
Cell<Type>::~Cell() {}
  
template <typename Type>
const Type& Cell<Type>::getInfo() const { return _info; }

template <typename Type>
Cell<Type>* Cell<Type>::getPred() const { return _pred; }

template <typename Type>
Cell<Type>* Cell<Type>::getSucc() const { return _succ; }
  
template <typename Type>
void Cell<Type>::setInfo(const Type& info) { _info = info; }

template <typename Type>
void Cell<Type>::setPred(Cell<Type>* pred) { _pred = pred; }

template <typename Type>
void Cell<Type>::setSucc(Cell<Type>* succ) { _succ = succ; }