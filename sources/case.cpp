#include "case.h"

Case::Case ()
{
  _couleur = BLANC;
  _vide = true;
}

Case::Case (bool vide, Couleur couleur) : _vide(vide), _couleur(couleur) 
{
  if (estVide()) _couleur = BLANC;
}

bool Case::estVide() const 
{ 
  return _vide; 
}

Case::Couleur Case::getCouleur() const 
{ 
  return _couleur;
}

Pixel::Color Case::getCouleurPixel() const
{
  Pixel::Color couleur;
  
  switch (_couleur)
  {
    case BLANC :
      couleur = Pixel::BWHITE;
      break;
      
    case NOIR :
      couleur = Pixel::WBLACK;
      break;
      
    case BLEU :
      couleur = Pixel::WBLUE;
      break;
      
    case JAUNE :
      couleur = Pixel::BYELLOW;
      break;
      
    case VERT :
      couleur = Pixel::BGREEN;
      break;
      
    case ROUGE :
      couleur = Pixel::BRED;
  }
  
  return couleur;
}
    
void Case::setCouleur(Couleur couleur)
{
  _couleur = couleur;
}

void Case::setVide(bool vide)
{
  _vide = vide;
  if (vide) _couleur = BLANC;
}